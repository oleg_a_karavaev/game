﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Pair<T, U>
{
    public Pair()
    {
    }

    public Pair(T first, U second)
    {
        this.First = first;
        this.Second = second;
    }

    public T First { get; set; }
    public U Second { get; set; }
};

public enum LandTypeEnum {Ebog, Emountains, Ehills, Efield, Eforest, Ewater, Eriver, Eempty};

public struct LandTypesProbabilityStruct
{
    public int Bog;
    public int Mountains;
    public int Hills;
    public int Field;
    public int Forest;
    public int Water;
    public int River;

    public void DecrType(LandTypeEnum landType, int val)
    {
        switch (landType)
        {
            case LandTypeEnum.Ebog:
                Bog -= val;
                break;
            case LandTypeEnum.Emountains:
                Mountains -= val;
                break;
            case LandTypeEnum.Ehills:
                Hills -= val;
                break;
            case LandTypeEnum.Efield:
                Field -= val;
                break;
            case LandTypeEnum.Eforest:
                Forest -= val;
                break;
            case LandTypeEnum.Ewater:
                Water -= val;
                break;
            case LandTypeEnum.Eriver:
                River -= val;
                break;
        }
    }

    public Dictionary<LandTypeEnum, int> GetParamCollection()
    {
        return new Dictionary<LandTypeEnum, int> {
              { LandTypeEnum.Ebog, Bog }
            , {LandTypeEnum.Emountains, Mountains }
            , {LandTypeEnum.Ehills, Hills }
            , {LandTypeEnum.Efield, Field }
            , {LandTypeEnum.Eforest, Forest }
            , {LandTypeEnum.Ewater, Water }
            , {LandTypeEnum.Eriver, River }
        };
    }

    public Dictionary<LandTypeEnum, int> GetNonZeroParamCollection()
    {
        var paramCol = GetParamCollection();
        Dictionary<LandTypeEnum, int> nzParamCol = new Dictionary<LandTypeEnum, int>();
        foreach (var p in paramCol)
        {
            if (p.Value > 0)
                nzParamCol.Add(p.Key, p.Value);
        }
        return nzParamCol;
    }

    public static LandTypesProbabilityStruct Dummy()
    {
        LandTypesProbabilityStruct tmp = new LandTypesProbabilityStruct();
        tmp.Bog = 0;
        tmp.Mountains = 0;
        tmp.Hills = 0;
        tmp.Field = 0;
        tmp.Forest = 0;
        tmp.Water = 0;
        tmp.River = 0;
        return tmp;
    }

    public bool IsDummy()
    {
        return
                   Bog == 0
                && Mountains == 0
                && Hills == 0
                && Field == 0
                && Forest == 0
                && Water == 0
                && River == 0;
    }

    public static Pair<LandTypesProbabilityStruct, LandTypesProbabilityStruct> GetDifference(LandTypesProbabilityStruct ltp1, LandTypesProbabilityStruct ltp2)
    {
        LandTypesProbabilityStruct tmp1 = LandTypesProbabilityStruct.Dummy();
        LandTypesProbabilityStruct tmp2 = LandTypesProbabilityStruct.Dummy();
        if (ltp1.Bog + ltp2.Bog > 0)
        {
            if (ltp1.Bog == 0)
                tmp2.Bog = ltp2.Bog;
            else
                tmp1.Bog = ltp1.Bog;
        }
        if (ltp1.Mountains + ltp2.Mountains > 0)
        {
            if (ltp1.Mountains == 0)
                tmp2.Mountains = ltp2.Mountains;
            else
                tmp1.Mountains = ltp1.Mountains;
        }
        if (ltp1.Hills + ltp2.Hills > 0)
        {
            if (ltp1.Hills == 0)
                tmp2.Hills = ltp2.Hills;
            else
                tmp1.Hills = ltp1.Hills;
        }
        if (ltp1.Field + ltp2.Field > 0)
        {
            if (ltp1.Field == 0)
                tmp2.Field = ltp2.Field;
            else
                tmp1.Field = ltp1.Field;
        }
        if (ltp1.Forest + ltp2.Forest > 0)
        {
            if (ltp1.Forest == 0)
                tmp2.Forest = ltp2.Forest;
            else
                tmp1.Forest = ltp1.Forest;
        }
        if (ltp1.Water + ltp2.Water > 0)
        {
            if (ltp1.Water == 0)
                tmp2.Water = ltp2.Water;
            else
                tmp1.Water = ltp1.Water;
        }
        if (ltp1.River + ltp2.River > 0)
        {
            if (ltp1.River == 0)
                tmp2.River = ltp2.River;
            else
                tmp1.River = ltp1.River;
        }
        return new Pair<LandTypesProbabilityStruct, LandTypesProbabilityStruct>(tmp1, tmp2);
    }

    public LandTypesProbabilityStruct Normilize()
    {
        int sum = Bog + Mountains + Hills + Field + Forest + Water + River;
        LandTypesProbabilityStruct tmp = new LandTypesProbabilityStruct();
        tmp.Bog = Bog * 100 / sum;
        tmp.Mountains = Mountains * 100 / sum;
        tmp.Hills = Hills * 100 / sum;
        tmp.Field = Field * 100 / sum;
        tmp.Forest = Forest * 100 / sum;
        tmp.Water = Water * 100 / sum;
        tmp.River = River * 100 / sum;
        return tmp;
    }

    public LandTypesProbabilityStruct SolveMost()
    {
        int sum = Bog + Mountains + Hills + Field + Forest + Water + River;
        LandTypesProbabilityStruct tmp = LandTypesProbabilityStruct.Dummy();
        List<int> vals = new List<int>() { Bog * 100 / sum, Mountains * 100 / sum, Hills * 100 / sum, Field * 100 / sum, Forest * 100 / sum, Water * 100 / sum, River * 100 / sum };
        Dictionary<int, int> valsRes = new Dictionary<int, int>();
        for (int i = 0; i < vals.Count; i++)
        {
            if (vals[i] > 0)
                valsRes.Add(i, vals[i]);
        }
        if (valsRes.Count == 0)
            throw new Exception("Undefined Land type");
        int key = 0;
        bool isSingle = false;
        if (valsRes.Count == 1)
        {
            isSingle = true;
            foreach (var k in valsRes.Keys)
            {
                key = k;
                break;
            }
        }
        else if (valsRes.Count == 2)
        {
            foreach (var kv in valsRes)
            {
                if (kv.Value >= 70)
                {
                    isSingle = true;
                    key = kv.Key;
                    break;
                }
            }
        }
        else if (valsRes.Count > 2)
        {
            foreach (var kv in valsRes)
            {
                if (kv.Value >= 50)
                {
                    isSingle = true;
                    key = kv.Key;
                    break;
                }
            }
        }
        if (isSingle)
        {
            switch (key)
            {
                case 0:
                    tmp.Bog = 100;
                    break;
                case 1:
                    tmp.Mountains = 100;
                    break;
                case 2:
                    tmp.Hills = 100;
                    break;
                case 3:
                    tmp.Field = 100;
                    break;
                case 4:
                    tmp.Forest = 100;
                    break;
                case 5:
                    tmp.Water = 100;
                    break;
                case 6:
                    tmp.River = 100;
                    break;
                default:
                    throw new Exception("Undefined Land type");
            }
            return tmp;
        }
        else
        {
            sum = 0;
            foreach (var v in valsRes.Values)
                if (v > 14)
                    sum = sum + v;
            foreach (var kv in valsRes)
            {
                if (kv.Value > 14)
                {
                    switch (kv.Key)
                    {
                        case 0:
                            tmp.Bog = kv.Value * 100 / sum;
                            break;
                        case 1:
                            tmp.Mountains = kv.Value * 100 / sum;
                            break;
                        case 2:
                            tmp.Hills = kv.Value * 100 / sum;
                            break;
                        case 3:
                            tmp.Field = kv.Value * 100 / sum;
                            break;
                        case 4:
                            tmp.Forest = kv.Value * 100 / sum;
                            break;
                        case 5:
                            tmp.Water = kv.Value * 100 / sum;
                            break;
                        case 6:
                            tmp.River = kv.Value * 100 / sum;
                            break;
                        default:
                            throw new Exception("Undefined Land type");
                    }
                }
            }
            return tmp;
        }
    }

    public void Add(LandTypesProbabilityStruct val)
    {
        Forest += val.Forest;
        River += val.River;
        Field += val.Field;
        Hills += val.Hills;
        Water += val.Water;
        Bog += val.Bog;
        Mountains += val.Mountains;
    }

    public void Add(LandTypesProbabilityStruct val, int weight)
    {
        Forest += val.Forest * weight / 100;
        River += val.River * weight / 100;
        Field += val.Field * weight / 100;
        Hills += val.Hills * weight / 100;
        Water += val.Water * weight / 100;
        Bog += val.Bog * weight / 100;
        Mountains += val.Mountains * weight / 100;
    }

    public void Assign(LandTypeEnum landType, int val)
    {

        switch (landType)
        {
            case LandTypeEnum.Ebog:
                Bog = val;
                break;
            case LandTypeEnum.Efield:
                Field = val;
                break;
            case LandTypeEnum.Eforest:
                Forest = val;
                break;
            case LandTypeEnum.Ehills:
                Hills = val;
                break;
            case LandTypeEnum.Emountains:
                Mountains = val;
                break;
            case LandTypeEnum.Eriver:
                River = val;
                break;
            case LandTypeEnum.Ewater:
                Water = val;
                break;
        }
    }
}

/// <summary>
/// КАласс состояния поля боя
/// </summary>
public class BattleFieldState : MonoBehaviour
{
    public static Pair<int, int> BattleGridSize { get; set; }
    public static Dictionary<int, Dictionary<int, LandBase>> BattleGrid { get; set; }

    private static List<KeyValuePair<LandTypeEnum, int>> GetSortedNonZeroLandParams(List<LandTypesProbabilityStruct> sectors)
    {
        LandTypesProbabilityStruct tmp = LandTypesProbabilityStruct.Dummy();
        foreach (var s in sectors)
        {
            tmp.Add(s);
        }
        tmp = tmp.Normilize().SolveMost();
        var tmpCol = tmp.GetParamCollection();
        List<KeyValuePair<LandTypeEnum, int>> nonZeroParam = new List<KeyValuePair<LandTypeEnum, int>>();
        foreach (var p in tmpCol)
        {
            if (p.Value > 0)
                nonZeroParam.Add(p);
        }
        nonZeroParam.Sort((x, y) => { if (x.Value > y.Value) return -1; else if (x.Value < y.Value) return 1; else return 0; });
        return nonZeroParam;
    }

    public static LandTypeEnum MergeSectorsType(List<LandTypesProbabilityStruct> sectors)
    {
        var nonZeroParam = GetSortedNonZeroLandParams(sectors);
        return nonZeroParam[0].Key;
    }

    public static LandTypesProbabilityStruct MergeSectors(List<LandTypesProbabilityStruct> sectors)
    {
        var nonZeroParam = GetSortedNonZeroLandParams(sectors);
        var tmp = LandTypesProbabilityStruct.Dummy();
        tmp.Assign(nonZeroParam[0].Key, nonZeroParam[0].Value);
        return tmp;
    }

    /// <summary>
    /// Метода определяет число секторов каждого вида поверхности
    /// </summary>
    /// <param name="land">Структра распределения видов поверхности</param>
    /// <param name="sectorsNum">Общее число секторов</param>
    /// <returns></returns>
    public static LandTypesProbabilityStruct CalculateSectors(LandTypesProbabilityStruct land, int sectorsNum)
    {
        var landN = land.Normilize();
        int sn = sectorsNum;
        int river;
        if (landN.River > 0)
        {
            landN.River = 0;
            landN = landN.Normilize();
            sn = sectorsNum - 7;
            river = 7;
        }
        else
            river = 0;
        List<int> valList = new List<int>() { landN.Forest, landN.Field, landN.Hills, landN.Water, landN.Bog, landN.Mountains };
        List<int> valRes = new List<int>();
        foreach (var val in valList)
        {
            if (val > 0)
            {
                int tmp = val * sn / 100;
                valRes.Add(tmp);
                sn -= tmp;
            }
            else
                valRes.Add(0);
        }
        LandTypesProbabilityStruct tmpLand = new LandTypesProbabilityStruct();
        tmpLand.River = river;
        tmpLand.Forest = valRes[0];
        tmpLand.Field = valRes[1];
        tmpLand.Hills = valRes[2];
        tmpLand.Water = valRes[3];
        tmpLand.Bog = valRes[4];
        tmpLand.Mountains = valRes[5];
        return tmpLand;
    }

    public static bool SuroundHave(Pair<int, int> sector, LandTypeEnum landType, out List<Pair<int, int>> correspSectors)
    {
        List<LandTypeEnum> surroundingSectors = new List<LandTypeEnum>() {
              BattleGrid[sector.First-1][sector.Second-1].LendType
            , BattleGrid[sector.First][sector.Second-1].LendType
            , BattleGrid[sector.First+1][sector.Second-1].LendType
            , BattleGrid[sector.First-1][sector.Second].LendType
            , BattleGrid[sector.First+1][sector.Second].LendType
            , BattleGrid[sector.First-1][sector.Second+1].LendType
            , BattleGrid[sector.First][sector.Second+1].LendType
            , BattleGrid[sector.First+1][sector.Second+1].LendType
        };
        bool exist = surroundingSectors.Contains(landType);
        correspSectors = null;
        if (exist)
        {
            correspSectors = new List<Pair<int, int>>();
            foreach (var lx in BattleGrid)
            {
                foreach (var ly in lx.Value)
                {
                    if (ly.Value.LendType == landType && sector.First != lx.Key && sector.Second != ly.Key && Math.Abs(sector.First - lx.Key) <= 1 && Math.Abs(sector.Second - ly.Key) <= 1)
                        correspSectors.Add(new Pair<int, int>(lx.Key, ly.Key));
                }
            }
        }
        return exist;
    }

    private static void FillBattleSectors(ref List<Pair<int, int>> sectors, int offsMin, int offsMax, ref int curLandTypeNum, LandTypeEnum targetLandType)
    {
        System.Random rand = new System.Random();
        Pair<int, int> tmp = null;
        while (curLandTypeNum > 0)
        {
            List<Pair<int, int>> correspSectors = null;
            tmp = null;
            while ((!sectors.Contains(tmp) && !SuroundHave(tmp, targetLandType, out correspSectors)) || tmp == null)
                tmp = new Pair<int, int>(rand.Next(sectors[0].Second + offsMin, sectors[sectors.Count - 1].Second), rand.Next(0, BattleGrid[0].Count - 1) - offsMax);
            BattleGrid[tmp.First][tmp.Second].LendType = targetLandType;
            curLandTypeNum--;
            int ixRem = sectors.FindIndex(x => x.First == tmp.First && x.Second == tmp.Second);
            sectors.RemoveAt(ixRem);
            int step = 1;
            if (curLandTypeNum > sectors.Count / 2)
                step = 4;
            else if (curLandTypeNum > sectors.Count / 4)
                step = 5;
            if ((targetLandType == LandTypeEnum.Emountains || targetLandType == LandTypeEnum.Ewater || targetLandType == LandTypeEnum.Ehills) && step > 1)
                step -= 2;
            for (int i = 0; i < step; i++)
            {
                correspSectors = null;
                if (SuroundHave(tmp, LandTypeEnum.Eempty, out correspSectors))
                {
                    tmp = null;
                    while (!sectors.Contains(tmp) || tmp == null)
                    {
                        List<Pair<int, int>> sectorsLocal = new List<Pair<int, int>>(sectors);
                        List<Pair<int, int>> localCorrespSectors = null;
                        tmp = correspSectors.Find(x => sectorsLocal.Contains(x));
                        if (SuroundHave(tmp, targetLandType, out localCorrespSectors) && localCorrespSectors.Count > 1)
                        {
                            correspSectors.Remove(tmp);
                            tmp = null;
                        }
                    }
                    if (tmp != null)
                    {
                        BattleGrid[tmp.First][tmp.Second].LendType = targetLandType;
                        curLandTypeNum--;
                        ixRem = sectors.FindIndex(x => x.First == tmp.First && x.Second == tmp.Second);
                        sectors.RemoveAt(ixRem);
                    }
                    else
                        break;
                }
                else
                    break;
            }
        }
    }

    /// <summary>
    /// Назначение секторам сетки поля боя видов поверхности
    /// </summary>
    /// <param name="surroundingArea">9 точек области столкновения. 4 - точка расположения защищающегося, остальные - окружающие. 3 - откуда наступает атакующий</param>
    /// <param name="playerInAttack">флаг о том, что игрок нападает</param>
    /// <param name="lastAttackerPositionArea">параметры точки, откуда пришёл атакующий (пока используется отдельно, позже надо вернуться к этому вопросу)</param>
    public static void FillBattleFieldGrid(List<LandTypesProbabilityStruct> surroundingArea, bool playerInAttack, LandTypesProbabilityStruct lastAttackerPositionArea)
    {
        if (surroundingArea.Count != 9)
            throw new Exception("The surroundingArea argument must have 9 elements");
        // Определения преимуществ сторон
        bool attackerAdvantage = false;
        bool defenderAdvantage = false;
        var diffPair = LandTypesProbabilityStruct.GetDifference(surroundingArea[4].SolveMost(), lastAttackerPositionArea.SolveMost());
        if (!(diffPair.First.IsDummy() && diffPair.Second.IsDummy()))
        {
            defenderAdvantage =
                   diffPair.First.Mountains > 0
                || diffPair.First.River > 0
                || diffPair.Second.Bog > 0
                || diffPair.First.Water > 0
                || (diffPair.First.Hills > 0 && diffPair.Second.Mountains == 0)
            ;
            attackerAdvantage = diffPair.First.River == 0 && (
                   diffPair.Second.Mountains > 0
                || diffPair.First.Bog > 0
                || (diffPair.Second.Hills > 0 && diffPair.First.Mountains == 0)
            );
        }
        // Разбиение поля боя на сектора в соответствии с преимуществами
        List<Pair<int, int>> leftSectors = new List<Pair<int, int>>();
        List<Pair<int, int>> rightSectors = new List<Pair<int, int>>();
        List<Pair<int, int>> topSectors = new List<Pair<int, int>>();
        List<Pair<int, int>> bottomSectors = new List<Pair<int, int>>();
        List<Pair<int, int>> centerSector = new List<Pair<int, int>>();
        if (!(defenderAdvantage || attackerAdvantage))
        {
            for (int i = 1; i < 8; i++)
            {
                int jbrd;
                if (i < 3)
                {
                    jbrd = i + 2;
                    for (int j = jbrd; j < 15 - (jbrd - 1); j++)
                    {
                        topSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else if (i > 5)
                {
                    jbrd = 8 - i + 2;
                    for (int j = jbrd; j < 15 - (jbrd - 1); j++)
                    {
                        bottomSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else
                {
                    jbrd = 5;
                    for (int j = 5; j < jbrd + 1 + 5 + Math.Abs(4 - i); j++)
                    {
                        rightSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                for (int j = 1; j < jbrd; j++)
                {
                    leftSectors.Add(new Pair<int, int>(i, j));
                    rightSectors.Add(new Pair<int, int>(i, 15 - j));
                }
            }
        }
        else if (defenderAdvantage || (attackerAdvantage && diffPair.First.Bog > 0 && diffPair.Second.Forest == 0))
        {
            int jbrd;
            for (int i = 1; i < 8; i++)
            {
                if (i < 3)
                {
                    jbrd = i + 2;
                    for (int j = jbrd; j < 10; j++)
                    {
                        topSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else if (i > 5)
                {
                    jbrd = 8 - i + 2;
                    for (int j = jbrd; j < 11; j++)
                    {
                        bottomSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else
                {
                    jbrd = 10 + i / 5;
                }
                for (int j = 1; j < jbrd; j++)
                {
                    leftSectors.Add(new Pair<int, int>(i, j));
                }
                for (int j = 10 + i / 5; j < 15; j++)
                {
                    rightSectors.Add(new Pair<int, int>(i, j));
                }
            }
        }
        else
        {
            centerSector.Add(new Pair<int, int>(4, 7));
            int jrbrd, jlbrd;
            for (int i = 1; i < 8; i++)
            {
                if (i < 5)
                    jrbrd = 7 + i % 2;
                else
                    jrbrd = 8;
                if (i < 3)
                {
                    jlbrd = i + 2;
                    for (int j = jlbrd; j < jrbrd; j++)
                    {
                        topSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else if (i > 5)
                {
                    jlbrd = 8 - i + 2;
                    for (int j = jlbrd; j < jrbrd; j++)
                    {
                        bottomSectors.Add(new Pair<int, int>(i, j));
                    }
                }
                else
                {
                    jlbrd = jrbrd;
                }
                for (int j = 1; j < jlbrd; j++)
                {
                    leftSectors.Add(new Pair<int, int>(i, j));
                }
                for (int j = jrbrd; j < 15; j++)
                {
                    if (!(i == 4 && j == 7))
                        rightSectors.Add(new Pair<int, int>(i, j));
                }
            }
        }
        // Если середина выделена отдельно, то тип поверхности - мёрж всего
        if (centerSector.Count > 0)
            BattleGrid[centerSector[0].First][centerSector[0].Second].LendType = MergeSectorsType(surroundingArea);

        // Получение общих параметров для областей поля боя
        LandTypesProbabilityStruct left, right, top, bottom;
        left = surroundingArea[3];
        left.Add(surroundingArea[0], 20);
        left.Add(surroundingArea[6], 20);
        left = left.Normilize();
        right = surroundingArea[4];
        right.Add(surroundingArea[5], 30);
        right.Add(surroundingArea[2], 20);
        right.Add(surroundingArea[8], 20);
        right = right.Normilize();
        top = surroundingArea[1];
        top = top.Normilize();
        top.Add(surroundingArea[0], 80);
        top.Add(surroundingArea[2], 80);
        bottom = surroundingArea[7];
        bottom.Add(surroundingArea[6], 80);
        bottom.Add(surroundingArea[8], 80);
        bottom = bottom.Normilize();

        // Подсчёт числа секторов конкретного типа в каждой из 4 областей поля боя (середина, если есть, то 1 и определяется общим мёржем)
        LandTypesProbabilityStruct sectorsInLeftField = CalculateSectors(left, leftSectors.Count);
        LandTypesProbabilityStruct sectorsInRightField = CalculateSectors(right, rightSectors.Count);
        LandTypesProbabilityStruct sectorsInTopField = CalculateSectors(top, topSectors.Count);
        LandTypesProbabilityStruct sectorsInBottomField = CalculateSectors(bottom, bottomSectors.Count);

        if (attackerAdvantage)
        {
            if (sectorsInLeftField.Mountains > 0)
            {
                sectorsInLeftField.DecrType(LandTypeEnum.Ehills, -sectorsInLeftField.Mountains);
                sectorsInLeftField.DecrType(LandTypeEnum.Emountains, sectorsInLeftField.Mountains);
            }
            if (sectorsInLeftField.River > 0)
            {
                sectorsInLeftField.DecrType(LandTypeEnum.Ewater, -sectorsInLeftField.River / 2);
                sectorsInLeftField.DecrType(LandTypeEnum.Efield, -(sectorsInLeftField.River - sectorsInLeftField.River / 2));
                sectorsInLeftField.DecrType(LandTypeEnum.Eriver, sectorsInLeftField.River);
            }
            if (sectorsInRightField.Bog > 0 && sectorsInRightField.Field >= sectorsInRightField.Bog / 3)
            {
                sectorsInRightField.DecrType(LandTypeEnum.Efield, sectorsInRightField.Bog / 3);
                sectorsInRightField.DecrType(LandTypeEnum.Ebog, -sectorsInRightField.Bog / 3);
            }
        }

        int leftAll = leftSectors.Count;
        int rightAll = rightSectors.Count;
        int topAll = topSectors.Count;
        int bottomAll = bottomSectors.Count;
        
        leftSectors.Sort((x, y) => x.Second.CompareTo(y.Second));
        rightSectors.Sort((x, y) => x.Second.CompareTo(y.Second));
        topSectors.Sort((x, y) => x.Second.CompareTo(y.Second));
        bottomSectors.Sort((x, y) => x.Second.CompareTo(y.Second));

        if (!defenderAdvantage)
        {
            while(leftSectors.Count > 0)
            {
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (rightSectors.Count > 0)
            {
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (topSectors.Count > 0)
            {
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (bottomSectors.Count > 0)
            {
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
        }
        else
        {
            LandTypeEnum border = LandTypeEnum.Eempty;
            int borderNum = 0;
            if (sectorsInRightField.River > 0)
            {
                border = LandTypeEnum.Eriver;
                borderNum = sectorsInRightField.River;
            }
            else if (sectorsInRightField.Mountains > 0)
            {
                border = LandTypeEnum.Emountains;
                borderNum = sectorsInRightField.Mountains;
            }
            if (border != LandTypeEnum.Eempty)
            {
                List<Pair<int, int>> leftestRight = new List<Pair<int, int>>();
                for (int i = 0; i < BattleGridSize.First; i++)
                    leftestRight.Add(rightSectors[i]);
                int placeTop = BattleGridSize.First - (BattleGridSize.First / 3) * 3;
                if (placeTop == 0)
                    placeTop = 1;
                int placeMiddle = BattleGridSize.First / 3;
                int curY = 0;
                int brd = placeTop;
                int ixRem = 0;
                while (curY < BattleGridSize.First)
                {
                    while (curY < brd)
                    {
                        BattleGrid[leftestRight[curY].First][leftestRight[curY].Second].LendType = border;
                        sectorsInRightField.DecrType(border, 1);
                        ixRem = rightSectors.FindIndex(x => x.First == leftestRight[curY].First && x.Second == leftestRight[curY].Second);
                        rightSectors.RemoveAt(ixRem);
                        curY++;
                    }
                    var tmp = sectorsInRightField;
                    tmp.DecrType(border, borderNum);
                    var likeabridge = MergeSectorsType(new List<LandTypesProbabilityStruct>() { tmp });
                    BattleGrid[leftestRight[curY].First][leftestRight[curY].Second].LendType = likeabridge;
                    sectorsInRightField.DecrType(likeabridge, 1);
                    ixRem = rightSectors.FindIndex(x => x.First == leftestRight[curY].First && x.Second == leftestRight[curY].Second);
                    rightSectors.RemoveAt(ixRem);
                    curY++;
                    brd = curY + placeMiddle;
                }
            }
            while (leftSectors.Count > 0)
            {
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref leftSectors, 2, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (rightSectors.Count > 0)
            {
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref rightSectors, 0, 2, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (topSectors.Count > 0)
            {
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref topSectors, 0, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
            while (bottomSectors.Count > 0)
            {
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Mountains, LandTypeEnum.Emountains);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Hills, LandTypeEnum.Ehills);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Bog, LandTypeEnum.Ebog);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Forest, LandTypeEnum.Eforest);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Water, LandTypeEnum.Ewater);
                FillBattleSectors(ref bottomSectors, 0, 0, ref sectorsInLeftField.Field, LandTypeEnum.Efield);
            }
        }
    }

    /// <summary>
    /// Построение сетки поля боя
    /// </summary>
    /// <param name="shiftX">Начальная координата Х поля боя</param>
    /// <param name="shiftY">Начальная координата Y поля боя</param>
    /// <param name="xSize">Число шестиугольников по X</param>
    /// <param name="ySize">Число шестиугольников по Y</param>
    /// <param name="hexagonSideX">Длина стороны шестиугольника-сектора поля боя (не радиус, а именно сторона)</param>
    public static void BuildBattleFieldGrid(int shiftX, int shiftY, int xSize, int ySize, int hexagonSideX)
    {
        BattleGrid = new Dictionary<int, Dictionary<int, LandBase>>();
        int hexagonStepX = hexagonSideX;
        float hexagonStepYFloat = hexagonStepX * 1.74f;
        BattleGridSize = new Pair<int, int>(ySize, xSize);
        int startPosX = shiftX;
        int startPosY = shiftY;
        int curX;
        float curY;
        for (int i = 0; i < ySize; i++)
        {
            BattleGrid[i] = new Dictionary<int, LandBase>();
            curY = startPosY + i * hexagonStepYFloat;
            for (int j = 0; j < xSize; j++)
            {
                curX = startPosX + j*hexagonStepX;
                LandBase tmp = new LandBase();
                tmp.transform.position = new Vector3(curX, curY, 0);
                BattleGrid[i][j] = tmp;
            }
            if ((i & 1) > 0)
                startPosX -= hexagonStepX;
            else
                startPosX += hexagonStepX;
        }
    }

    /// <summary>
    /// Юниты в бою
    /// </summary>
    public static ArrayList BattleUnits { get; set; }
    /// <summary>
    /// Метод олучения ближайших юнитов к заданному
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public static ArrayList GetNearestBattleUnits(BattleUnitBase unit)
    {
        throw new System.Exception("not iimplemented yet");
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
