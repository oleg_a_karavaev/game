﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    /// <summary>
    /// Класс триггера условий воздействия юнита или на юнита
    /// </summary>
    public class Trigger : ITrigger
    {
        /// <summary>
        /// Список юнита для которого генерируется событие
        /// </summary>
        public BattleUnitBase TriggeredUnit { get; set; }
        /// <summary>
        /// Событие в атаке
        /// </summary>
        public bool IsAttack { get; set; }
        /// <summary>
        /// Событие в защите
        /// </summary>
        public bool IsDefense { get; set; }
        /// <summary>
        /// Событие в активности юнита (он атакует, либо событие по условию атаки на юнита)
        /// </summary>
        public bool IsAction { get; set; }

        /// <summary>
        /// Конструктор. Инициализация обработчиков событий, привязка юнита-хозяина, установка типа триггера
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="byAction"></param>
        public Trigger(BattleUnitBase unit, bool byAction = false)
        {
            TriggeredUnit = unit;
            if (byAction) //триггер активных действий
            {
                IsAction = true;
                if (TriggeredUnit != null)
                    TriggeredUnit.ActionTriggered += ActionHandler;
            }
            else // триггер по выполнению порогового условия на начало хода
            {
                IsAction = false;
                GlobalScenarioState.turnTrigger.Triggered += NextTurnHandler;
            }
        }
        /// <summary>
        /// Проверка выполнения порогового условия
        /// </summary>
        /// <returns></returns>
        public bool TriggerCondition()
        {
            // по уровню здоровья
            return (int)(TriggeredUnit.SolveParameters().helth / TriggeredUnit.CurrentHelth * 100) < threshold;
        }
        /// <summary>
        /// порог срабаывания
        /// </summary>
        private int threshold;
        /// <summary>
        /// установить порог срабатывания
        /// </summary>
        /// <param name="val"></param>
        public void SetThreshold(int val)
        {
            threshold = val;
        }
        /// <summary>
        /// Обработчик события начала нового хода
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextTurnHandler(object sender, TriggerEvent e)
        {
            if (TriggerCondition()) // если выполняется пороговое условие
            {
                TriggerEvent NextTurnTriggeredEvent = new TriggerEvent();
                NextTurnTriggeredEvent.IsAction = false;
                NextTurnTriggeredEvent.IsAttack = false;
                NextTurnTriggeredEvent.IsDefense = false;
                NextTurnTriggeredEvent.TargetBattleUnits = null;
                NextTurnTriggeredEvent.TriggeredUnit = this.TriggeredUnit;
                OnTriggered(NextTurnTriggeredEvent);
            }
        }
        /// <summary>
        /// Обработчик события активных действий юнита или против него
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActionHandler(object sender, TriggerEvent e)
        {
            if (IsAction && e.IsAction && ((IsAttack && e.IsAttack) || (IsDefense && e.IsDefense))) // событие соответствует текущему триггеру
            {
                TriggerEvent ActionTriggeredEvent = new TriggerEvent();
                ActionTriggeredEvent.IsAction = true;
                ActionTriggeredEvent.IsAttack = IsAttack;
                ActionTriggeredEvent.IsDefense = IsDefense;
                ActionTriggeredEvent.TargetBattleUnits = e.TargetBattleUnits;
                ActionTriggeredEvent.TriggeredUnit = sender as BattleUnitBase;
                OnTriggered(ActionTriggeredEvent);
            }
        }

        /// <summary>
        /// делагат обработчика событий срабатывания триггера
        /// </summary>
        public event EventHandler<TriggerEvent> Triggered;
        /// <summary>
        /// Вызов обработчиков событий срабатывания триггера
        /// </summary>
        /// <param name="e"></param>
        private void OnTriggered(TriggerEvent e)
        {
            EventHandler<TriggerEvent> handler = Triggered;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
