using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��� ������� � ����
/// </summary>
[Flags]
public enum FractionEnum {
      player = 0x0001
    , evilTroops = 0x0002
    , bandits = 0x0004
    , gaia = 0x0008
    , civilians = 0x0010
    , forestDwellers = 0x0020
    , forestDwellersPerson = 0x0040
    , civiliansPerson = 0x0080
}
/// <summary>
/// ������� ����� ��� ���� ������
/// </summary>
public class UnitBase : MonoBehaviour
{

    protected uint id;
    /// <summary>
    /// ���������� ������������� �����
    /// </summary>
    public uint Id { get { return id; } }

    protected UnitParameters unitParameters;
    /// <summary>
    /// ��������� �����
    /// </summary>
    public virtual UnitParameters UnitParameters { get { return unitParameters; } }

    protected FractionEnum fraction;
    /// <summary>
    /// ������� �����
    /// </summary>
    public virtual FractionEnum Fraction { get { return fraction; } }

    protected virtual void Start () {
    }
	
	// Update is called once per frame
	protected virtual void Update () {
	}
	
}

public class UnitInitiatedEventArgs : EventArgs
{
    FractionEnum fraction;
}