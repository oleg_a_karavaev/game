﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System.Collections.Generic;
using System;

/// <summary>
/// Базовый класс для юнитов на поле боя
/// </summary>
public class BattleUnitBase : UnitBase, IFly
{

    protected const int sleepId = 0;
    protected const int stunedId = 1;
    protected const int maimedId = 2;
    protected const int poisonedId = 3;
    protected const int hiddenId = 5;
    protected const int frozenId = 4;

    protected bool sleep;
    protected bool stuned;
    protected bool maimed;
    protected bool poisoned;
    protected bool hidden;
    protected bool frozen;

    public event EventHandler<TriggerEvent> ActionTriggered;
    private void OnActionTriggered(TriggerEvent e)
    {
        EventHandler<TriggerEvent> handler = ActionTriggered;
        if (handler != null)
        {
            handler(this, e);
        }
    }


    protected ArrayList sleepCounters = new ArrayList();
    protected ArrayList stunedCounters = new ArrayList();
    protected ArrayList maimedCounters = new ArrayList();
    protected ArrayList poisonedCounters = new ArrayList();
    protected ArrayList hiddenCounters = new ArrayList();
    protected ArrayList frozenCounters = new ArrayList();

    /// <summary>
    /// Свойство-флаг сна
    /// </summary>
    public bool Sleep { get { return sleep; } }
    /// <summary>
    /// Свойство-флаг оглушения
    /// </summary>
    public bool Stuned { get { return stuned; } }
    /// <summary>
    /// Свойство-флаг калечения
    /// </summary>
    public bool Maimed { get { return maimed; } }
    /// <summary>
    /// Свойство-флаг отравления
    /// </summary>
    public bool Poisoned { get { return poisoned; } }
    /// <summary>
    /// Свойство-флаг невидимости
    /// </summary>
    public bool Hidden { get { return hidden; } }
    /// <summary>
    /// Свойство-флаг заморозки
    /// </summary>
    public bool Frozen { get { return frozen; } }

    /// <summary>
    /// Усыпление юнита до конца боя
    /// </summary>
    public void SetSleep()
    {
        sleep = true;
        Counter cnt = new Counter();
        cnt.Init(true, sleepId, this.Id);
        sleepCounters.Add(cnt);
    }
    /// <summary>
    /// Оглушение юнита до конца боя
    /// </summary>
    public void SetStuned()
    {
        stuned = true;
        Counter cnt = new Counter();
        cnt.Init(true, stunedId, this.Id);
        stunedCounters.Add(cnt);
    }
    /// <summary>
    /// Калечение юнита до конца боя
    /// </summary>
    public void SetMaimed()
    {
        maimed = true;
        Counter cnt = new Counter();
        cnt.Init(true, maimedId, this.Id);
        maimedCounters.Add(cnt);
    }
    /// <summary>
    /// Отравление юнита до конца боя
    /// </summary>
    public void SetPoisoned()
    {
        poisoned = true;
        Counter cnt = new Counter();
        cnt.Init(true, poisonedId, this.Id);
        poisonedCounters.Add(cnt);
    }
    /// <summary>
    /// Юнит не видим до конца боя
    /// </summary>
    public void SetHidden()
    {
        hidden = true;
        Counter cnt = new Counter();
        cnt.Init(true, hiddenId, this.Id);
        hiddenCounters.Add(cnt);
    }
    /// <summary>
    /// Заморозить юнита до конца боя
    /// </summary>
    public void SetFrozen()
    {
        frozen = true;
        Counter cnt = new Counter();
        cnt.Init(true, frozenId, this.Id);
        frozenCounters.Add(cnt);
    }
    /// <summary>
    /// Усыпить юнита на число ходов
    /// </summary>
    /// <param name="timer">Число ходов, на которые юнит будет усыплён</param>
    public void SetSleep(int timer)
    {
        sleep = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, sleepId, this.Id);
        sleepCounters.Add(cnt);
    }
    /// <summary>
    /// Оглушить юнита на число ходов
    /// </summary>
    /// <param name="timer">Число ходов, на которые юнит будет оглушён</param>
    public void SetStuned(int timer)
    {
        stuned = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, stunedId, this.Id);
        stunedCounters.Add(cnt);
    }
    /// <summary>
    /// Покалечить юнита на число ходов
    /// </summary>
    /// <param name="timer">Число ходов, в течение которого юнит будет покалечен</param>
    public void SetMaimed(int timer)
    {
        maimed = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, maimedId, this.Id);
        maimedCounters.Add(cnt);
    }
    /// <summary>
    /// Отравление юнита на число ходов
    /// </summary>
    /// <param name="timer">Число ходов, в течение которых юнит будет отравленным</param>
    public void SetPoisoned(int timer)
    {
        poisoned = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, poisonedId, this.Id);
        poisonedCounters.Add(cnt);
    }
    /// <summary>
    /// Сделать юнита не видимым в течение заданного числа ходов
    /// </summary>
    /// <param name="timer">Число ходов невидимости юнита</param>
    public void SetHidden(int timer)
    {
        hidden = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, hiddenId, this.Id);
        hiddenCounters.Add(cnt);
    }
    /// <summary>
    /// Заморозить юнита в течение заданного числа ходов
    /// </summary>
    /// <param name="timer">Длительность заморозки юнита в ходах</param>
    public void SetFrozen(int timer)
    {
        frozen = true;
        Counter cnt = new Counter();
        cnt.Init(true, timer, frozenId, this.Id);
        frozenCounters.Add(cnt);
    }

    protected int curHelth;
    /// <summary>
    /// Текущий уровень здоровья
    /// </summary>
    public int CurrentHelth { get { return curHelth; } }
    protected int curManna;
    /// <summary>
    /// Текущий уровень манны/выносливости
    /// </summary>
    public int CurrentManna { get { return curManna; } }

    protected Dictionary<int, int> activeSkills;
    protected Dictionary<int, int> activeReturnSkills;
    protected Dictionary<int, int> summoningSkills;
    protected Dictionary<int, int> passiveSkills;
    protected Dictionary<int, int> auraSkills;
    protected Dictionary<int, int> UnitTransformationSkills;
    protected Dictionary<int, int> stateModificationSkills;
    protected Dictionary<int, Counter> activatedAuraSkills;

    protected int unitTransformationSkillId;
    protected bool unitTransformationSkillActivated;
    /// <summary>
    /// Идентификатор скила трансформации юнита (для оборотней)
    /// </summary>
    public int UnitTransformationSkillId { get { return unitTransformationSkillId; } }
    /// <summary>
    /// Свойство-флаг активации скила трансформации у юнита
    /// </summary>
    public bool UnitTransformationSkillActivated { get { return unitTransformationSkillActivated; } }

    /// <summary>
    /// Обьект, в который произошла трансформация
    /// </summary>
    public BattleUnitBase UnitTransformation { get; set; }


    protected List<SkillTrigger> triggerSkills;
    /// <summary>
    /// Скилы (экземпляры), срабатывающие по условию (порог здоровья на начало хода)
    /// </summary>
    public List<SkillTrigger> TriggerSkills { get { return triggerSkills; } }
    /// <summary>
    /// Задать скилы, срабатывающие по условию
    /// </summary>
    /// <param name="skills"></param>
    public void SetTriggerSkills(IEnumerable<SkillTrigger> skills)
    {
        triggerSkills.AddRange(skills);
        foreach(var skill in skills)
        {
            skill.SkillTriggered += SkillTriggeredHandler;
        }
    }
    /// <summary>
    /// Обработчик события срабатывания условия включения скила, срабатывающего по условию
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SkillTriggeredHandler(object sender, SkillTriggeredEvent e)
    {
        SkillShadows ss = e.SkillId as SkillShadows;
        if (ss != null)
        {
            if (ss.Activated)
                ss.SwitchPositions();
            else
                ss.MakeShadows();
        }
        else
        {
            SkillSummoning ssm = e.SkillId as SkillSummoning;
            if (ssm != null && ssm.IsTransformationSkill())
            {
                unitTransformationSkillId = e.SkillId.Id;
                ActivateUnitTransformationSkill(unitTransformationSkillId);
            }
            else
            {
                SkillStateModification sm = e.SkillId as SkillStateModification;
                if (sm != null)
                {
                    ActivateStateModificationSkill(e.SkillId.Id);
                }
                else
                {
                    SkillAura sa = e.SkillId as SkillAura;
                    if (sa != null)
                        ActivateAuraSkill(e.SkillId.Id);
                    else
                    {
                        SkillPassive sp = e.SkillId as SkillPassive;
                        if (sp != null)
                        {
                            passiveSkills.Add(sp.Id, 1);
                        }
                        else
                        {
                            throw new Exception("Not implemented yet");
                        }
                    }
                }
            }
        }

    }

    /// <summary>
    /// Обработчик события окончания счёта ходов (счёт активируется при включении ауры, или смене состояния юнита: усыпление, оглушение и т.д.)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void CountCompleteHandler(object sender, CountCompleteEventArgs e)
    {
        if (e.CallerId == this.Id)
        {
            if (e.IsEffect)
            {
                switch(e.RefId)
                {
                    case sleepId:
                        sleepCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    case stunedId:
                        stunedCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    case maimedId:
                        maimedCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    case poisonedId:
                        poisonedCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    case frozenId:
                        frozenCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    case hiddenId:
                        hiddenCounters.Remove(sender);
                        ((Counter)sender).Dispose();
                        break;
                    default:
                        ((Counter)sender).Dispose();
                        break;
                }
                sleep = sleepCounters.Count == 0;
                stuned = stunedCounters.Count == 0;
                maimed = maimedCounters.Count == 0;
                poisoned = poisonedCounters.Count == 0;
                frozen = frozenCounters.Count == 0;
                hidden = hiddenCounters.Count == 0;
            }
            else
            {
                if (activatedAuraSkills.ContainsKey(e.RefId))
                {
                    activatedAuraSkills[e.RefId].Dispose();
                    activatedAuraSkills.Remove(e.RefId);
                }
            }
        }
    }

    /// <summary>
    /// Активировать скил ауры по идентификатору скила
    /// </summary>
    /// <param name="id">Идентификатор скила ауры</param>
    /// <returns>Скил найден и активирован</returns>
    public bool ActivateAuraSkill (int id)
    {
        if (!auraSkills.ContainsKey(id))
            return false;
        if (activatedAuraSkills.ContainsKey(id))
            activatedAuraSkills[id].Init(false, GlobalScenarioState.auraSkills[id].Duration, id, this.Id);
        else
        {
            Counter cnt = new Counter();
            cnt.Init(false, GlobalScenarioState.auraSkills[id].Duration, id, this.Id);
            activatedAuraSkills.Add(id, cnt);
        }
        return true;
    }
    /// <summary>
    /// Активировать скил модификации состояния: невидимость, полёт и т.п.
    /// </summary>
    /// <param name="id">идентификатор скила</param>
    /// <returns>Скил найден и активирован</returns>
    public bool ActivateStateModificationSkill(int id)
    {
        if (!stateModificationSkills.ContainsKey(id))
            return false;
        throw new Exception("not implemented yet. It must change a unit state (Hidden, Resurect, Stuned, Sleep, etc.)");
    }
    /// <summary>
    /// Активировать скил трансформации юнита
    /// </summary>
    /// <param name="id">Идентификатор скила</param>
    /// <returns>Скил найден и активирован</returns>
    public bool ActivateUnitTransformationSkill(int id)
    {
        if (!UnitTransformationSkills.ContainsKey(id))
            return false;
        unitTransformationSkillActivated = true;
        unitTransformationSkillId = id;
        UnitTransformation = GlobalScenarioState.summoningSkills[id].GetSummoningUnit();
        return true;
    }

    protected bool isSummoningUnit;
    /// <summary>
    /// Флаг о том, что этот юнит является вызванным скилом призыва
    /// </summary>
    public bool IsSummoningUnit { get { return isSummoningUnit; } }

    /// <summary>
    /// Определить юнит как призванный скилом
    /// </summary>
    public void MarkAsSummoning()
    {
        isSummoningUnit = true;
    }
    /// <summary>
    /// Получить список всех скилов как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скилы найдены</returns>
    public bool GetSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (activeSkills != null && activeSkills.Count > 0)
            foreach (var skill in activeSkills.Keys)
                skills.Add(new SkillStruct(skill, activeSkills[skill]));
        if (passiveSkills != null && passiveSkills.Count > 0)
            foreach (var skill in passiveSkills.Keys)
                skills.Add(new SkillStruct(skill, passiveSkills[skill]));
        if (auraSkills != null && auraSkills.Count > 0)
            foreach (var skill in auraSkills.Keys)
                skills.Add(new SkillStruct(skill, auraSkills[skill]));
        if (activeReturnSkills != null && activeReturnSkills.Count > 0)
            foreach (var skill in activeReturnSkills.Keys)
                skills.Add(new SkillStruct(skill, activeReturnSkills[skill]));
        if (summoningSkills != null && summoningSkills.Count > 0)
            foreach (var skill in summoningSkills.Keys)
                skills.Add(new SkillStruct(skill, summoningSkills[skill]));
        if (stateModificationSkills != null && stateModificationSkills.Count > 0)
            foreach (var skill in stateModificationSkills.Keys)
                skills.Add(new SkillStruct(skill, stateModificationSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список активных (атака, лечение, ...) скилов как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetActiveSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (activeSkills != null && activeSkills.Count > 0)
            foreach (var skill in activeSkills.Keys)
                skills.Add(new SkillStruct(skill, activeSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список активных (атака, лечение, ...) скилов с эффектом возврата как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetActiveReturnSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (activeReturnSkills != null && activeReturnSkills.Count > 0)
            foreach (var skill in activeReturnSkills.Keys)
                skills.Add(new SkillStruct(skill, activeReturnSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список призывающих скилов как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetSummoningSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (summoningSkills != null && summoningSkills.Count > 0)
            foreach (var skill in summoningSkills.Keys)
                skills.Add(new SkillStruct(skill, summoningSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список пассивных скилов как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetPassiveSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (passiveSkills != null && passiveSkills.Count > 0)
            foreach (var skill in passiveSkills.Keys)
                skills.Add(new SkillStruct(skill, passiveSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список скилов ауры как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetAuraSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (auraSkills != null && auraSkills.Count > 0)
            foreach (var skill in auraSkills.Keys)
                skills.Add(new SkillStruct(skill, auraSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список скилов изменения состояния юнита как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetStateModificationSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (stateModificationSkills != null && stateModificationSkills.Count > 0)
            foreach (var skill in stateModificationSkills.Keys)
                skills.Add(new SkillStruct(skill, stateModificationSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить список активированных скилов ауры как контейнер пар (иденттификатор скила, уровень скила)
    /// </summary>
    /// <param name="skills">ссылка на контейер для скилов</param>
    /// <returns>Скил найден</returns>
    public bool GetActivatedAuraSkills(ref ArrayList skills)
    {
        if (skills == null)
            skills = new ArrayList();
        else
            skills.Clear();
        if (activatedAuraSkills != null && auraSkills.Count > 0)
            foreach (var skill in activatedAuraSkills.Keys)
                skills.Add(new SkillStruct(skill, auraSkills[skill]));
        return skills.Count > 0;
    }
    /// <summary>
    /// Получить скил по идентификатору как пару (идентификатор, уровень)
    /// </summary>
    /// <param name="skillId">идентификатор скила</param>
    /// <returns>Структура: идентификатор скила, уровень</returns>
    public SkillStruct GetSkill(int skillId)
    {
        if (activeSkills != null && activeSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, activeSkills[skillId]);
        if (passiveSkills != null && passiveSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, passiveSkills[skillId]);
        if (auraSkills != null && auraSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, auraSkills[skillId]);
        if (activeReturnSkills != null && activeReturnSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, activeReturnSkills[skillId]);
        if (summoningSkills != null && summoningSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, summoningSkills[skillId]);
        if (stateModificationSkills != null && stateModificationSkills.ContainsKey(skillId))
            return new SkillStruct(skillId, stateModificationSkills[skillId]);
        return new SkillStruct(0, 0);
    }
    /// <summary>
    /// Повышение уровня скила по идентификатору
    /// </summary>
    /// <param name="skillId">идентификатор скила</param>
    /// <returns>Скил найден и уровень повышен</returns>
    public bool SkillLevelUp(int skillId)
    {
        if (activeSkills != null && activeSkills.ContainsKey(skillId))
        {
            activeSkills[skillId]++;
            return true;
        }
        if (passiveSkills != null && passiveSkills.ContainsKey(skillId))
        {
            passiveSkills[skillId]++;
            return true;
        }
        if (auraSkills != null && auraSkills.ContainsKey(skillId))
        {
            auraSkills[skillId]++;
            return true;
        }
        if (activeReturnSkills != null && activeReturnSkills.ContainsKey(skillId))
        {
            activeReturnSkills[skillId]++;
            return true;
        }
        if (summoningSkills != null && summoningSkills.ContainsKey(skillId))
        {
            summoningSkills[skillId]++;
            return true;
        }
        if (stateModificationSkills != null && stateModificationSkills.ContainsKey(skillId))
        {
            stateModificationSkills[skillId]++;
            return true;
        }
        return false;
    }
    /// <summary>
    /// Получить параметры скила по идентификатору
    /// </summary>
    /// <param name="id">идентификатор скила</param>
    /// <returns>Структура параметров скила</returns>
    public ParametersStruct GetSkillParameters(int id)
    {
        if (activeSkills != null && activeSkills.ContainsKey(id))
        {
            return GlobalScenarioState.activeSkills[id].SkillParameters.Scale(100 + 20 * activeSkills[id]);
        }
        if (passiveSkills != null && passiveSkills.ContainsKey(id))
        {
            return GlobalScenarioState.passiveSkills[id].SkillParameters.Scale(100 + 20 * activeSkills[id]); ;
        }
        if (auraSkills != null && auraSkills.ContainsKey(id))
        {
            return GlobalScenarioState.auraSkills[id].SkillParameters.Scale(100 + 20 * auraSkills[id]);
        }
        if (activeReturnSkills != null && activeReturnSkills.ContainsKey(id))
        {
            return GlobalScenarioState.activeReturnSkills[id].SkillParameters.Scale(100 + 20 * activeReturnSkills[id]);
        }
        return ParametersStruct.Dummy();
    }
    /// <summary>
    /// Получить параметры возврата от скила с эффектом возрата по идентификатору скила
    /// </summary>
    /// <param name="id">идентификатор скила</param>
    /// <returns>Структура параметров скила</returns>
    public ParametersStruct GetSkillReturnParameters(int id)
    {
        if (activeReturnSkills != null && activeReturnSkills.ContainsKey(id))
        {
            return GlobalScenarioState.activeReturnSkills[id].SkillReturnParameters.Scale(100 + 20 * activeReturnSkills[id]);
        }
        return ParametersStruct.Dummy();
    }

    protected bool canFlay = false;

    protected bool broken = false;
    /// <summary>
    /// Флаг поломки снаряжения
    /// </summary>
    public bool Broken { get { return broken; } }
    /// <summary>
    /// Установить флаг поломки снаряжения
    /// </summary>
    public virtual void MarkAsBroken()
    {
        broken = true;
        unitParameters.SetScale(40);
    }
    /// <summary>
    /// Перезагрузить здоровье и манну из параметров юнита (максимальные значения этих параметров)
    /// </summary>
    public virtual void ReloadHM()
    {
        ParametersStruct tmp = SolveParameters();
        if (tmp.helth > this.curHelth)
            this.curHelth = tmp.helth;
        if (tmp.manna > this.curManna)
            this.curManna = tmp.manna;
    }
    /// <summary>
    /// Перезагрузить здоровье из параметров юнита (максимальные значения параметра)
    /// </summary>
    public virtual void ReloadHelth()
    {
        ParametersStruct tmp = SolveParameters();
        if (tmp.helth > this.curHelth)
            this.curHelth = tmp.helth;
    }
    /// <summary>
    /// Перезагрузить манну из параметров юнита (максимальные значения параметра)
    /// </summary>
    public virtual void ReloadManna()
    {
        ParametersStruct tmp = SolveParameters();
        if (tmp.manna > this.curManna)
            this.curManna = tmp.manna;
    }
    /// <summary>
    /// Починить снаряжение
    /// </summary>
    public virtual void Repair()
    {
        broken = false;
        unitParameters.SetScale(100);
    }
    /// <summary>
    /// Разрешить все текущие эффекты и бонусы на параметры юнита
    /// </summary>
    /// <returns></returns>
    public virtual ParametersStruct SolveParameters()
    {
        return UnitParameters.BattleParameters.SolveParameters(null);
    }
    /// <summary>
    /// Получить параметры юнита по маске
    /// </summary>
    /// <param name="mask"></param>
    /// <returns></returns>
    public virtual ParametersStruct ParametersMasked(int mask)
    {
        ParametersStruct tmp = ParametersStruct.Dummy();
        tmp.Assign(SolveParameters(), mask);
        return tmp;
    }
    /// <summary>
    /// Получиить параметры юнита. Возвращает экземпляр класса параметров
    /// </summary>
    public override UnitParameters UnitParameters { get { return unitParameters; } }

    /// <summary>
    /// Включить возможность полёта
    /// </summary>
    public void FlyPossibilitySwichOn()
    {
        canFlay = true;
    }
    /// <summary>
    /// Отключить возможность полёта
    /// </summary>
    public void FlyPossibilitySwichOff()
    {
        canFlay = false;
    }
    /// <summary>
    /// Проверить возможность юнита летать
    /// </summary>
    /// <returns>Когда летает - true</returns>
    public bool CanFlay()
    {
        return canFlay;
    }
}
