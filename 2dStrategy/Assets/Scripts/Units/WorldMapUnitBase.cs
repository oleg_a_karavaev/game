﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Базовый класс для юнитов на карте игры
/// </summary>
public class WorldMapUnitBase : UnitBase
{
    /// <summary>
    /// Параметры юнита. Для уровня карты мира это не имеет значения, поэтому возращает null
    /// </summary>
    public override UnitParameters UnitParameters { get { return null; } }
    /// <summary>
    /// Структура параметров класса юнита в соответствии с типом. Пусто, т.к. не имеет смысла
    /// </summary>
    public virtual ParametersStruct Parameters { get { return ParametersStruct.Dummy(); } }

}