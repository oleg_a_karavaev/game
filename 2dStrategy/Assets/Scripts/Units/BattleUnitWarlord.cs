﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// Базовый класс для юнитов героев на поле боя
/// </summary>
public class BattleUnitWarlord : BattleUnitBase
{
    private const int amuletsNumber = 4;

    protected List<ItemBase> haversack;
    /// <summary>
    /// Инвентарь
    /// </summary>
    public List<ItemBase> Haversack { get { return haversack; } }

    protected ItemVestmentHead itemHead;
    protected ItemVestmentTorso itemTorso;
    protected ItemVestmentShoulders itemShoulders;
    protected ItemVestmentForearmArm itemForearmArm;
    protected ItemVestmentLegs itemLegs;
    protected ItemVestmentFoots itemFoots;
    protected ItemSupportNeck itemNeck;
    protected ItemSupportFingers itemFingersR;
    protected ItemSupportFingers itemFingersL;
    protected ItemSupportAmuletes[] itemAmuletes = new ItemSupportAmuletes[amuletsNumber];
    protected ItemSupportCloak itemCloak;
    protected ItemSupportHood itemHood;
    protected ItemArmBase itemArmR;
    protected ItemArmBase itemArmL;
    /* доступно только 1 из 3, доступность определяется классом юнита */
    protected ItemAuxiliaryRunestone itemRunestone; // рунный камень с уникальными заклятиями, доп. заклятия, или увеличение силы имеющихся
    protected ItemAuxiliaryDagger itemDagger; // кинжал/короткий мечь для лучников и некоторых магов - увеличивает возможности в ближнем бою
    protected ItemAuxiliaryThrowing itemPilum; // для воинов ближнего боя. Пилум - можно бросить на некоторое расстояние

    /// <summary>
    /// Снаряжение для головы
    /// </summary>
    public ItemVestmentHead ItemHead { get { return itemHead; } }
    /// <summary>
    /// Снаряжение для торса
    /// </summary>
    public ItemVestmentTorso ItemTorso { get { return itemTorso; } }
    /// <summary>
    /// Снаряжение для плечей: наплечники, обручи и т.п.
    /// </summary>
    public ItemVestmentShoulders ItemShoulders { get { return itemShoulders; } }
    /// <summary>
    /// Снаряжение для предплечья/ладоней - перчатки, рукавицы и т.п. + наручи
    /// </summary>
    public ItemVestmentForearmArm ItemForearmArm { get { return itemForearmArm; } }
    /// <summary>
    /// Снаряжение для ног: штаны, бронетрусы :) и т.д.
    /// </summary>
    public ItemVestmentLegs ItemLegs { get { return itemLegs; } }
    /// <summary>
    /// Снаряжение для ступней: сапоги, сандали, обмотки, ...
    /// </summary>
    public ItemVestmentFoots ItemFoots { get { return itemFoots; } }
    /// <summary>
    /// Снаряжение для шея: обереги, и прочее
    /// </summary>
    public ItemSupportNeck ItemNeck { get { return itemNeck; } }
    /// <summary>
    /// Снаряжение для пальцев правой руки: кольца, перстни
    /// </summary>
    public ItemSupportFingers ItemFingersR { get { return itemFingersR; } }
    /// <summary>
    /// Снаряжение для пальцев левой руки: кольца, перстни
    /// </summary>
    public ItemSupportFingers ItemFingersL { get { return itemFingersL; } }
    /// <summary>
    /// Обереги, не надеваемые на шею: камни, фигурки и прочее
    /// </summary>
    public ItemSupportAmuletes[] ItemAmuletes { get { return itemAmuletes; } }
    /// <summary>
    /// Плащ, шкура
    /// </summary>
    public ItemSupportCloak ItemCloak { get { return itemCloak; } }
    /// <summary>
    /// Капюшон, шапка
    /// </summary>
    public ItemSupportHood ItemmHood { get { return itemHood; } }
    /// <summary>
    /// Снаряжение в правую руку
    /// </summary>
    public ItemArmBase ItemArmR { get { return itemArmR; } }
    /// <summary>
    /// Снаряжение в левую руку
    /// </summary>
    public ItemArmBase ItemArmL { get { return itemArmL; } }
    /// <summary>
    /// Вспомогательное снаряжение (не ломается): рунные камни, кинжалы, короткие мечи, метательные топоры, метательные копья (вроде пилума).
    /// </summary>
    public ItemAuxiliaryBase ItemAuxiliary
    {
        get
        {
            if (itemRunestone != null)
                return itemRunestone;
            if (itemDagger != null)
                return itemDagger;
            if (itemPilum != null)
                return itemPilum;
            return null;
        }
    }
    /// <summary>
    /// Сломать всё снаряжение
    /// </summary>
    public override void MarkAsBroken()
    {
        if (itemHead != null) itemHead.MarkAsBroken();
        if (itemTorso != null) itemTorso.MarkAsBroken();
        if (itemShoulders != null) itemShoulders.MarkAsBroken();
        if (itemForearmArm != null) itemForearmArm.MarkAsBroken();
        if (itemLegs != null) itemLegs.MarkAsBroken();
        if (itemFoots != null) itemFoots.MarkAsBroken();
        if (itemNeck != null) itemNeck.MarkAsBroken();
        if (itemFingersR != null) itemFingersR.MarkAsBroken();
        if (itemFingersL != null) itemFingersL.MarkAsBroken();
        for (int i = 0; i < amuletsNumber; i++)
            if (itemAmuletes[i] != null) itemAmuletes[i].MarkAsBroken();
        if (itemCloak != null) itemCloak.MarkAsBroken();
        if (itemHood != null) itemHood.MarkAsBroken();
        if (itemArmR != null) itemArmR.MarkAsBroken();
        if (itemArmL != null) itemArmL.MarkAsBroken();
        if (itemRunestone != null) itemRunestone.MarkAsBroken();
        if (itemDagger != null) itemDagger.MarkAsBroken();
        if (itemPilum != null) itemPilum.MarkAsBroken();
    }
    /// <summary>
    /// Починить всё снаряжение
    /// </summary>
    public override void Repair()
    {
        broken = false;
        if (itemHead != null) itemHead.Repair();
        if (itemTorso != null) itemTorso.Repair();
        if (itemShoulders != null) itemShoulders.Repair();
        if (itemForearmArm != null) itemForearmArm.Repair();
        if (itemLegs != null) itemLegs.Repair();
        if (itemFoots != null) itemFoots.Repair();
        if (itemNeck != null) itemNeck.Repair();
        if (itemFingersR != null) itemFingersR.Repair();
        if (itemFingersL != null) itemFingersL.Repair();
        for (int i = 0; i < amuletsNumber; i++)
            if (itemAmuletes[i] != null) itemAmuletes[i].Repair();
        if (itemCloak != null) itemCloak.Repair();
        if (itemHood != null) itemHood.Repair();
        if (itemArmR != null) itemArmR.Repair();
        if (itemArmL != null) itemArmL.Repair();
        if (itemRunestone != null) itemRunestone.Repair();
        if (itemDagger != null) itemDagger.Repair();
        if (itemPilum != null) itemPilum.Repair();
    }
    /// <summary>
    /// Разрешить все текущие эффекты и бонусы на параметры юнита
    /// </summary>
    /// <returns></returns>
    public override ParametersStruct SolveParameters()
    {
        KeyValuePair<ParametersStruct, int>[] toAdd = new KeyValuePair<ParametersStruct, int>[14]
        {
              new KeyValuePair<ParametersStruct, int>(ItemHead != null ? ItemHead.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemTorso != null ? ItemTorso.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemShoulders != null ? ItemShoulders.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemForearmArm != null ? ItemForearmArm.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemLegs != null ? ItemLegs.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemFoots != null ? ItemFoots.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemNeck != null ? ItemNeck.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemFingersR != null ? ItemFingersR.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemFingersL != null ? ItemFingersL.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemCloak != null ? ItemCloak.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemmHood != null ? ItemmHood.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemArmR != null ? ItemArmR.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemArmL != null ? ItemArmL.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
            , new KeyValuePair<ParametersStruct, int>(ItemAuxiliary != null ? ItemAuxiliary.Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask())
        };
        Array.Resize(ref toAdd, 14 + amuletsNumber);
        for (int i = 0; i < amuletsNumber; i++)
        {
            toAdd[14 + i] = new KeyValuePair<ParametersStruct, int>(ItemAmuletes[i] != null ? ItemAmuletes[i].Parameters : ParametersStruct.Dummy(), ParameterMask.FullMask());
        }
        return UnitParameters.BattleParameters.SolveParameters(toAdd);
    }
    /// <summary>
    /// Получить параметры юнита по маске
    /// </summary>
    /// <param name="mask"></param>
    /// <returns></returns>
    public override ParametersStruct ParametersMasked(int mask)
    {
        ParametersStruct tmp = ParametersStruct.Dummy();
        tmp.Assign(SolveParameters(), mask);
        return tmp;
    }
}
