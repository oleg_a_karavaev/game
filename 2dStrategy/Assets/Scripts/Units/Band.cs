﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Базовый класс для отряда на карте мира
/// </summary>
public class Band : WorldMapUnitBase
{
    protected List<BattleUnitBase> squad;
    /// <summary>
    /// Состав отряда
    /// </summary>
    public List<BattleUnitBase> Squad { get { return squad; } }

    /// <summary>
    /// Фракция отряда
    /// </summary>
    public override FractionEnum Fraction
    {
        get
        {
            var wl = GetWarlord();
            if (wl != null)
                return wl.Fraction;
            return fraction;
        }
    }
    /// <summary>
    /// Структура параметров класса уровня с учётом текущего уровня. Определяется по герою в отряде.
    /// </summary>
    public override ParametersStruct Parameters
    {
        get
        {
            var wl = GetWarlord();
            if (wl != null)
                return wl.ParametersMasked(ParameterMask.speed);
            return base.Parameters;
        }
    }
    /// <summary>
    /// Добавить юнит в отряд
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public bool AddUnitToSquad(BattleUnitBase unit)
    {
        if (unit == null)
            throw new System.Exception("Adding unit is Null");
        BattleUnitWarlord warlord = unit as BattleUnitWarlord;
        if (warlord != null)
        {
            if (squad.Any(u => u is BattleUnitWarlord))
                return false;
            squad.Insert(0, unit);
            return true;
        }
        else
        {
            squad.Add(unit);
            return true;
        }
    }
    /// <summary>
    /// Возвращает ссылку на экземпляр героя
    /// </summary>
    /// <returns></returns>
    public BattleUnitWarlord GetWarlord()
    {
        var wl = from s in squad
                 where s is BattleUnitWarlord
                 select s as BattleUnitWarlord;
        if (wl == null || wl.Count() == 0)
            return null;
        if (wl.Count() > 1)
            throw new System.Exception("Squad contains more then 2 warlords");
        return wl.ElementAt(0);
    }
}
