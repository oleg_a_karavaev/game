﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    /// <summary>
    /// перечислимы тип отношений фракций
    /// </summary>
    public enum RelationsEnum : int
    {
          allies = 0x0000
        , neutral = 0x0001
        , enemies = 0x0002
    }

    /// <summary>
    /// Класс взаимоотношений в игре
    /// </summary>
    public class Relations
    {
        /// <summary>
        /// Массив [allies, neutral, enemies] словарей фракций и флага наличия соответствующих отношений
        /// </summary>
        static private Dictionary<FractionEnum, bool>[] globalFractionsRelations = { new Dictionary<FractionEnum, bool>(), new Dictionary<FractionEnum, bool>(), new Dictionary<FractionEnum, bool>() };
        /// <summary>
        /// Словарь отношений юнитов, представляющих самостоятельные фракции
        /// </summary>
        static private Dictionary<uint, Dictionary<uint, bool>> personal2PersonalRelations = new Dictionary<uint, Dictionary<uint, bool>>();
        /// <summary>
        /// Словарь отношения конкретного юнита, представляющего самостоятельную фракцию и других фракций
        /// </summary>
        static private Dictionary<uint, Dictionary<FractionEnum, bool>> personal2FractionsRelations = new Dictionary<uint, Dictionary<FractionEnum, bool>>();
        /// <summary>
        /// сервисный метод получения границ диапазона типа отношений фракций
        /// </summary>
        /// <returns></returns>
        private static KeyValuePair<int, int> GetBoardsRelationsEnum()
        {
            var relEnumArray = Enum.GetValues(typeof(RelationsEnum));
            return new KeyValuePair<int, int>((int)relEnumArray.GetValue(0), (int)relEnumArray.GetValue(relEnumArray.GetLength(0)));
        }
        /// <summary>
        /// Метод получения отношения между двмя юнитами
        /// </summary>
        /// <param name="unit1"></param>
        /// <param name="unit2"></param>
        /// <returns></returns>
        static public RelationsEnum GetRelation(UnitBase unit1, UnitBase unit2)
        {
            KeyValuePair<int, int> boards = GetBoardsRelationsEnum(); //получение границ диапазона
            for (int i = boards.Key; i <= boards.Value; i++) //поиск в пределах диапазона
            {
                //поиск true одновременно у фракций каждого юнита в соответствующем отношению элементе массива, если обе фракции там присутствуют
                if (globalFractionsRelations[i].ContainsKey(unit1.Fraction) && globalFractionsRelations[i].ContainsKey(unit2.Fraction))
                    if (globalFractionsRelations[i][unit1.Fraction] && globalFractionsRelations[i][unit1.Fraction])
                        return (RelationsEnum)i;
            }
            // одна или обе фракции могут быть персональными - юнит=фракция
            bool unit1IsPersonalFraction = unit1.Fraction == FractionEnum.civiliansPerson || unit1.Fraction == FractionEnum.forestDwellersPerson;
            bool unit2IsPersonalFraction = unit2.Fraction == FractionEnum.civiliansPerson || unit2.Fraction == FractionEnum.forestDwellersPerson;
            //аналогичный поиск по другим контенерам с персональными фракциями
            if (unit1IsPersonalFraction && unit2IsPersonalFraction)
            {
                for (int i = boards.Key; i <= boards.Value; i++)
                {
                    if (personal2PersonalRelations.ContainsKey(unit1.Id))
                    {
                        if (personal2PersonalRelations[unit1.Id].ContainsKey(unit2.Id) && personal2PersonalRelations[unit1.Id][unit2.Id])
                            return (RelationsEnum)i;
                    }
                    else
                    {
                        if (personal2PersonalRelations[unit2.Id].ContainsKey(unit1.Id) && personal2PersonalRelations[unit2.Id][unit1.Id])
                            return (RelationsEnum)i;
                    }
                }
            }
            if (unit1IsPersonalFraction)
            {
                for (int i = boards.Key; i <= boards.Value; i++)
                {
                    if (personal2FractionsRelations.ContainsKey(unit1.Id))
                    {
                        if (personal2FractionsRelations[unit1.Id].ContainsKey(unit2.Fraction) && personal2FractionsRelations[unit1.Id][unit2.Fraction])
                            return (RelationsEnum)i;
                    }
                }
            }
            else if (unit2IsPersonalFraction)
            {
                for (int i = boards.Key; i <= boards.Value; i++)
                {
                    if (personal2FractionsRelations.ContainsKey(unit2.Id))
                    {
                        if (personal2FractionsRelations[unit2.Id].ContainsKey(unit1.Fraction) && personal2FractionsRelations[unit2.Id][unit1.Fraction])
                            return (RelationsEnum)i;
                    }
                }
            }
            return RelationsEnum.neutral;
        }
    }
}
