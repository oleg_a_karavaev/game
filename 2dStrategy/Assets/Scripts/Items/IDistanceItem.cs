﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Items
{
    /// <summary>
    /// Интерфейс для снаряжения дальнего действия
    /// </summary>
    public interface IDistanceItem
    {
        /// <summary>
        /// Установить дальность действия
        /// </summary>
        /// <param name="val"></param>
        void SetDistance(int val);
        /// <summary>
        /// Получить дальность действия
        /// </summary>
        /// <returns></returns>
        int GetDistance();
    }
}
