﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts.Items;

/// <summary>
/// Базовый класс для копья. Дистанция от 2
/// </summary>
public class ItemArmWeaponSpear : ItemArmWeaponTwoHandedSteelArms, IDistanceItem
{
    protected int distance;
    /// <summary>
    /// Получить дальность действия
    /// </summary>
    /// <returns></returns>
    public virtual int GetDistance()
    {
        return distance;
    }
    /// <summary>
    /// Установить дальность действия
    /// </summary>
    /// <param name="val"></param>
    public virtual void SetDistance(int val)
    {
        distance = Math.Max(val, 2);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
