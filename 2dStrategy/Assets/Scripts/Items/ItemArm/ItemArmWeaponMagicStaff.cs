﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Items;

/// <summary>
/// Базовый класс для магических посохов
/// </summary>
public class ItemArmWeaponMagicStaff : ItemArmWeaponTwoHanded, IDistanceItem
{
    protected int distance;
    /// <summary>
    /// Получить дальность действия
    /// </summary>
    /// <returns></returns>
    public virtual int GetDistance()
    {
        return distance;
    }
    /// <summary>
    /// Установить дальность действия
    /// </summary>
    /// <param name="val"></param>
    public virtual void SetDistance(int val)
    {
        distance = val;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
