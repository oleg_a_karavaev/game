﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Перечислимый тип редкости снаряжения: обычное, редкое, уникальное, легендарное/былинное, богов (вроде меч богов, и т.п.)
/// </summary>
public enum ItemKindEnum { normal, rare, unique, legendary, gods }
/// <summary>
/// Базовый класс для снаряжения
/// </summary>
public class ItemBase : MonoBehaviour
{
    /// <summary>
    /// идентификатор снаряжения
    /// </summary>
    protected int id;
    /// <summary>
    /// Свойство. Идентификатор снаряжения
    /// </summary>
    public int Id { get { return id; } }
    /// <summary>
    /// имя снаряжения
    /// </summary>
    protected string itemName;
    /// <summary>
    /// описание снаряжения
    /// </summary>
    protected string itemDescription;
    /// <summary>
    /// редкость снаряжения
    /// </summary>
    protected ItemKindEnum itemKind;
    /// <summary>
    /// срок службы снаряжениея 
    /// </summary>
    protected int durability;
    /// <summary>
    /// текущий срок службы снаряжения. По истичении - ломается
    /// </summary>
    protected int durabilityCurrent;
    /// <summary>
    /// флаг поломки снаряжения
    /// </summary>
    protected bool broken = false;
    /// <summary>
    /// разрешение учёта износа снаряжения
    /// </summary>
    protected bool durabilityEn = false;
    /// <summary>
    /// Свойство. Получение текущего износа снаряжения. Если износ не учитывается, то всегда 100
    /// </summary>
    public int Durability { get { if (durabilityEn) return durabilityCurrent; else return 100; } }
    /// <summary>
    /// Свойство. Состояние: сломано, или нет
    /// </summary>
    public bool Broken { get { return broken; } }
    /// <summary>
    /// Определить как поломанное
    /// </summary>
    public void MarkAsBroken()
    {
        broken = true;
    }
    /// <summary>
    /// Починить
    /// </summary>
    public void Repair()
    {
        broken = false;
        durabilityCurrent = durability;
    }
    /// <summary>
    /// Включить учёт износа
    /// </summary>
    public void DurabilitySwitchOn()
    {
        durabilityEn = true;
    }
    /// <summary>
    /// Понизить текущий запас прочности на 1
    /// </summary>
    public void ApplyWear()
    {
        if (durabilityEn && !broken)
        {
            durabilityCurrent--;
            if (durabilityCurrent == 0) MarkAsBroken();
        }
    }
    /// <summary>
    /// Свойство. Имя снаряжения
    /// </summary>
    public string ItemName { get { return itemName; } }
    /// <summary>
    /// Свойство. Описание снаряжения
    /// </summary>
    public string ItemDescription { get { return itemDescription; } }
    /// <summary>
    /// Свойство. Редкость снаряжения
    /// </summary>
    public ItemKindEnum ItemKind { get { return itemKind; } }
    /// <summary>
    /// параметры снаряжения. Не уменьшаются при поломке
    /// </summary>
    protected ParametersStruct parameters;
    /// <summary>
    /// Свойство. Параметры снаряжения. При поломке возвращают 40% от начальных значений
    /// </summary>
    public ParametersStruct Parameters { get { if (Broken) return parameters.Scale(40); else return parameters; } }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
