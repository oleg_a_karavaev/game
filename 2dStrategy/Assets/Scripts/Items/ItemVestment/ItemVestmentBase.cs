﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Типа класса брони: колдунская, лёгкая, тяжёлая
/// </summary>
public enum VestmentTypeEnum { magic, light, heavy}

/// <summary>
/// Базовый класс для защитного одеваемого снаряжения (не щит)
/// </summary>
public class ItemVestmentBase : ItemBase
{
    /// <summary>
    /// класс снаряжения
    /// </summary>
    protected VestmentTypeEnum vestmentType;
    /// <summary>
    /// Свойство. Класс снаряжения
    /// </summary>
    public VestmentTypeEnum VestmentType { get { return vestmentType; } }

    // Use this for initialization
            void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
