﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс для зданий
/// </summary>
public class BuildingsBase : MonoBehaviour
{
    /// <summary>
    /// идентификатор здания
    /// </summary>
    protected int id;
    /// <summary>
    /// Свойство. Иднетификатор здания
    /// </summary>
    public int Id { get { return id; } }
    /// <summary>
    /// Имя типа здания
    /// </summary>
    public string BuildingTypeName { get; set; }
    /// <summary>
    /// Имя здания
    /// </summary>
    public string BuildingName { get; set; }
    /// <summary>
    /// Принадлежность здания к фракции
    /// </summary>
    public FractionEnum Fraction { get; set; }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
