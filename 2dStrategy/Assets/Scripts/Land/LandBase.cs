﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс для элементов ландшафта
/// </summary>
public class LandBase : MonoBehaviour
{
    protected LandTypeEnum landType = LandTypeEnum.Eempty;
    /// <summary>
    /// Свойство. Тип местности
    /// </summary>
    public LandTypeEnum LendType { get { return landType; } set { landType = value; } }
    /// <summary>
    /// стурктура параметров влияния на юниты
    /// </summary>
    protected ParametersStruct effects;
    /// <summary>
    /// Свойство. Стурктура параметров влияния на юниты
    /// </summary>
    public ParametersStruct Effects { get { return effects; } }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
