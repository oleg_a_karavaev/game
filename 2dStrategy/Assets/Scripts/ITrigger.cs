﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    /// <summary>
    /// Событие срабатывания условия воздействий юнитов и на юнитов
    /// </summary>
    public class TriggerEvent : EventArgs
    {
        /// <summary>
        /// Список юнитов целей
        /// </summary>
        public ArrayList TargetBattleUnits { get; set; }
        /// <summary>
        /// Список юнита для которого генерируется событие
        /// </summary>
        public BattleUnitBase TriggeredUnit { get; set; }
        /// <summary>
        /// Событие в атаке
        /// </summary>
        public bool IsAttack { get; set; }
        /// <summary>
        /// Событие в защите
        /// </summary>
        public bool IsDefense { get; set; }
        /// <summary>
        /// Событие в активности юнита (он атакует, либо событие по условию атаки на юнита)
        /// </summary>
        public bool IsAction { get; set; }
    }

    /// <summary>
    /// Интерфейс триггера условия воздействия юнита или на юнита
    /// </summary>
    public interface ITrigger
    {
        /// <summary>
        /// Список юнита для которого генерируется событие
        /// </summary>
        BattleUnitBase TriggeredUnit { get; set; }
        /// <summary>
        /// Проверка условия срабатывания триггера
        /// </summary>
        /// <returns></returns>
        bool TriggerCondition();
        /// <summary>
        /// Установить порог срабатывания триггера
        /// </summary>
        /// <param name="val"></param>
        void SetThreshold(int val);
        /// <summary>
        /// делегат обработчика события срабатывания триггера
        /// </summary>
        event EventHandler<TriggerEvent> Triggered;
        /// <summary>
        /// Событие в атаке
        /// </summary>
        bool IsAttack { get; set; }
        /// <summary>
        /// Событие в защите
        /// </summary>
        bool IsDefense { get; set; }
        /// <summary>
        /// Событие в активности юнита (он атакует, либо событие по условию атаки на юнита)
        /// </summary>
        bool IsAction { get; set; }
    }
}
