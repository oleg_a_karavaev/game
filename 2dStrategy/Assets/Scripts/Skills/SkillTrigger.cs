﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System;

/// <summary>
/// Событие срабатывания триггера вызова скила
/// </summary>
public class SkillTriggeredEvent : EventArgs
{
    /// <summary>
    /// Экземпляр активируемого скила
    /// </summary>
    public SkillBase SkillId { get; set; }
}
/// <summary>
/// Класс активации скилов по условию
/// </summary>
public class SkillTrigger : SkillBase
{
    /// <summary>
    /// Интерфейс триггера
    /// </summary>
    public ITrigger Trigger { get; set; }
    /// <summary>
    /// Активируемый скил
    /// </summary>
    public SkillBase Skill { get; set; }

    public SkillTrigger(ITrigger trigger, SkillBase skill)
    {
        Trigger = trigger;
        Skill = skill;
        Trigger.Triggered += TriggeredHandler;
    }
    /// <summary>
    /// Обработчик вызова скила по срабатыванию триггера
    /// </summary>
    public event EventHandler<SkillTriggeredEvent> SkillTriggered;
    /// <summary>
    /// Вызов обработчика
    /// </summary>
    /// <param name="e"></param>
    private void OnSkillTriggered(SkillTriggeredEvent e)
    {
        EventHandler<SkillTriggeredEvent> handler = SkillTriggered;
        if (handler != null)
        {
            handler(this, e);
        }
    }
    /// <summary>
    /// Обработчик срабатывания триггера
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void TriggeredHandler(object sender, TriggerEvent e)
    {
        SkillTriggeredEvent skillTriggeredEvent = new SkillTriggeredEvent();
        skillTriggeredEvent.SkillId = Skill;
        OnSkillTriggered(skillTriggeredEvent);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
