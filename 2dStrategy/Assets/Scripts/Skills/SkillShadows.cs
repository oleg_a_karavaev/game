﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Базовый класс для скилов порождения теней юнита (имеют те же параметры, но не умеют атаковать)
/// </summary>
public class SkillShadows : SkillBase, IDisposable
{
    /// <summary>
    /// Число теней
    /// </summary>
    public int ShadowsNumber { get; set; }
    /// <summary>
    /// Список созданных теней
    /// </summary>
    public List<BattleUnitBase> Shadows { get; set; }
    /// <summary>
    /// Скил призыва юнита для создания тени
    /// </summary>
    public SkillSummoning Skill { get; set; }
    /// <summary>
    /// Флаг активации скила
    /// </summary>
    public bool Activated { get; set; }
    /// <summary>
    /// Хозяин скила
    /// </summary>
    public BattleUnitBase BaseUnit { get; set; }

    /// <summary>
    /// Конструктор скила создания теней по условию состояния юнита
    /// </summary>
    /// <param name="num">число теней</param>
    /// <param name="skillSummoningId">идентификатор соответствующего призывающего скила</param>
    /// <param name="baseUnit">хозяин скила</param>
    public SkillShadows(int num, int skillSummoningId, BattleUnitBase baseUnit)
    {
        ShadowsNumber = num;
        Skill = GlobalScenarioState.summoningSkills[skillSummoningId];
        Activated = false;
        Shadows = null;
        BaseUnit = baseUnit;
        GlobalScenarioState.turnTrigger.Triggered += NextTurnHandler;
    }
    /// <summary>
    /// Обработчик события смены хода
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void NextTurnHandler(object sender, EventArgs e)
    {
        if (Activated)
        {
            SwitchPositions();
        }
    }

    /// <summary>
    /// Смена позиции вызывающего юнита и теней
    /// </summary>
    public void SwitchPositions()
    {
        throw new Exception("not implemented yed");
    }

    /// <summary>
    /// Создать тень
    /// </summary>
    public void MakeShadows()
    {
        Activated = true;
        Shadows = new List<BattleUnitBase>();
        for (int i = 0; i < ShadowsNumber; i++)
        {
            var tmp = Skill.GetSummoningUnit();
            tmp.UnitParameters.AssignMasked(BaseUnit.UnitParameters, ParameterMask.NoAttackMask());
            tmp.MarkAsSummoning();
            Shadows.Add(tmp);
        }
    }

    public void Dispose()
    {
        Activated = false;
        Shadows.Clear();
        Shadows = null;
        Skill = null;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
