﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс пассивных скилов. Эти скилы дают постоянные эффекты к характеристикам юнитов
/// </summary>
public class SkillPassive : SkillEffectBase
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
