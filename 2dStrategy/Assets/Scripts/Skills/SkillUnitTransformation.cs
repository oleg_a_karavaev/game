﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Скил превращения юнита
/// </summary>
/// <typeparam name="T"></typeparam>
public class SkillUnitTransformation<T> : SkillSummoning where T : BattleUnitBase, new()
{
    /// <summary>
    /// Юнит, в который выполняется превращение
    /// </summary>
    public T Unit { get { return new T(); } }

    public SkillUnitTransformation()
    { }
    /// <summary>
    /// Получить касс призываемого юнита
    /// </summary>
    /// <returns></returns>
    public override Type GetSummoningUnitClass()
    {
        return typeof(T);
    }
    /// <summary>
    /// Является ли призывающим скилом
    /// </summary>
    /// <returns></returns>
    public override bool IsSummoningSkill()
    {
        return true;
    }
    /// <summary>
    /// Является ли превращающим скилом
    /// </summary>
    /// <returns></returns>
    public override bool IsTransformationSkill()
    {
        return true;
    }
    /// <summary>
    /// Получения экземпляра юнита превращения
    /// </summary>
    /// <returns></returns>
    public override BattleUnitBase GetSummoningUnit()
    {
        var unit = Unit;
        unit.MarkAsSummoning();
        return unit;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
