﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс для активных скилов (атаки, и иные воздействия на юниты). Нет реализации для площади.
/// </summary>
public class SkillActive : SkillEffectBase
{
    /// <summary>
    /// Цели атаки
    /// </summary>
    public ArrayList Targets { get; set; }
    /// <summary>
    /// Нужно определить цели по условию
    /// </summary>
    public bool NeedGenerateTargets { get; set; }
    /// <summary>
    /// Цели - ближайшие юниты
    /// </summary>
    public bool NeedGetNearestTargets { get; set; }
    /// <summary>
    /// Метод получения коллекции целей для атаки
    /// </summary>
    /// <param name="unit">юнит, относительно которого надо выбирать цели</param>
    public void GenerateTargets(BattleUnitBase unit)
    {
        if (!NeedGenerateTargets)
            return;
        if (NeedGetNearestTargets && unit != null)
            Targets.AddRange(BattleFieldState.GetNearestBattleUnits(unit));
        else if (unit != null)
        {
            for (int i = 0; i < BattleFieldState.BattleUnits.Count; i++)
            {
                if (((BattleUnitBase)BattleFieldState.BattleUnits[i]).Fraction != unit.Fraction)
                    Targets.Add(BattleFieldState.BattleUnits[i]);
            }
        }
        else
            Targets.AddRange(BattleFieldState.BattleUnits);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
