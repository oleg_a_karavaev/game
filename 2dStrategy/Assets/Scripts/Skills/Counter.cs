﻿using UnityEngine;
using System.Collections;
using System;

public class CountCompleteEventArgs : EventArgs
{
    public int RefId { get; set; }
    public uint CallerId { get; set; }
    public bool IsEffect { get; set; }
}
/// <summary>
/// Класс счётчика для отсчёта числа ходов
/// </summary>
public class Counter : MonoBehaviour, IDisposable
{

    private int value = 0;
    private int refId = 0;
    private bool initiated = false;
    private bool infinit = false;
    private uint callerId;
    private bool isEffect = false;

    /// <summary>
    /// Инициализация счётчика с конечным числом ходов
    /// </summary>
    /// <param name="isEffect">Считает для эффекта (состояние юнита) от воздействия, не для ауры</param>
    /// <param name="initVal">Начальное значение (счёт вниз)</param>
    /// <param name="refId">идентификатор скила, или состояния(эффекта), для которого выполняется отсчёт</param>
    /// <param name="callerId">идентификатор юнита у которого вызван счётчик</param>
    public void Init(bool isEffect, int initVal, int refId, uint callerId)
    {
        this.refId = refId;
        value = initVal;
        initiated = true;
        infinit = false;
        this.callerId = callerId;
        this.isEffect = isEffect;
    }
    /// <summary>
    /// Инициализация ожидания до конца боя
    /// </summary>
    /// <param name="isEffect">Считает для эффекта (состояние юнита) от воздействия, не для ауры</param>
    /// <param name="refId">идентификатор скила, или состояния(эффекта), для которого выполняется отсчёт</param>
    /// <param name="callerId">идентификатор юнита у которого вызван счётчик</param>
    public void Init(bool isEffect, int refId, uint callerId)
    {
        this.refId = refId;
        value = 0;
        this.infinit = true;
        initiated = true;
        this.callerId = callerId;
        this.isEffect = isEffect;
    }
    /// <summary>
    /// Обработчик события конца хода
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void TurnEndHandler(object sender, EventArgs e)
    {
        if (initiated && !infinit)
        {
            value--;
            if (value == 0)
            {
                initiated = false;
                CountCompleteEventArgs countCompleteEvent = new CountCompleteEventArgs();
                countCompleteEvent.RefId = refId;
                countCompleteEvent.CallerId = callerId;
                countCompleteEvent.IsEffect = isEffect;
                OnCountComplete(countCompleteEvent);
            }
        }
    }
    /// <summary>
    /// Обработчик события конца боя
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void BattleCompleteHandler(object sender, EventArgs e)
    {
        initiated = false;
        infinit = false;
        CountCompleteEventArgs countCompleteEvent = new CountCompleteEventArgs();
        countCompleteEvent.RefId = refId;
        countCompleteEvent.CallerId = callerId;
        countCompleteEvent.IsEffect = isEffect;
        OnCountComplete(countCompleteEvent);
    }

    public event EventHandler<CountCompleteEventArgs> CountComplete;
    /// <summary>
    /// Вызов обработчика события окончания счёта
    /// </summary>
    /// <param name="e"></param>
    protected virtual void OnCountComplete(CountCompleteEventArgs e)
    {
        EventHandler< CountCompleteEventArgs> handler = CountComplete;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    public void Dispose()
    { }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
