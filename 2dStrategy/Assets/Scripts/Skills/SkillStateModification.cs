﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс скилов модификации состояния (на пример, заморозить, усыпить, покалечить, ...)
/// </summary>
public class SkillStateModification : SkillBase
{

    public int Duration { get; set; }
    public bool Infinit { get; set; }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
