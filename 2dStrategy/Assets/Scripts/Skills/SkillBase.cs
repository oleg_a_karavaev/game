﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Структура предачи информации идентификации скила
/// </summary>
public struct SkillStruct
{
    /// <summary>
    /// уровень скила
    /// </summary>
    public int level;
    /// <summary>
    /// идентификатор скила
    /// </summary>
    public int id;

    public SkillStruct(int id, int level)
    {
        this.id = id;
        this.level = level;
    }

    public SkillStruct(int id)
    {
        this.id = id;
        this.level = 1;
    }
}
/// <summary>
/// Базовый класс для сиклов. Для активации скилов при необходимости должны быть методы в родительских классов этих скилов
/// </summary>
public class SkillBase : MonoBehaviour
{
    protected int id;
    public int Id { get { return id; } }
    public string SkillName { get; set; }
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
