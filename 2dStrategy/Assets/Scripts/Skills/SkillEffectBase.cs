﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Базовый класс для скилов, оказывающих эффекты на парамретры юнитов (в т.ч. и урон здоровью/манне)
/// </summary>
public class SkillEffectBase : SkillBase
{

    protected ParametersStruct skillParameters;
    /// <summary>
    /// Эффект влияния на парарметры цели
    /// </summary>
    public ParametersStruct SkillParameters { get { return skillParameters; } }

    /// <summary>
    /// Получить параметры влияния по уровню скила
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public ParametersStruct SolveSkill(int level)
    {
        return SkillParameters.Scale(100 + 20 * level);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
