﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс скилов для аур
/// </summary>
public class SkillAura : SkillEffectBase
{

    /// <summary>
    /// флаг использования скила
    /// </summary>
    public bool SkillActive { get; set; }
    protected int duration;
    /// <summary>
    /// Длительнрость ауры в ходах
    /// </summary>
    public int Duration { get { return duration; } }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
