﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Класс для активных скилов с возратом (вампиризм и т.п.)
/// </summary>
public class SkillActiveReturn : SkillEffectBase
{
    protected ParametersStruct skillReturnParameters;
    /// <summary>
    /// Возращаемые вызывающему скил параметры
    /// </summary>
    public ParametersStruct SkillReturnParameters { get { return skillReturnParameters; } }

    protected int returnMask;
    /// <summary>
    /// Маска возращаемых параметров
    /// </summary>
    public int ReturnMask { get { return returnMask; } }

    /// <summary>
    /// использовать масштабирование параметров воздействия вместо конкретных параметров
    /// </summary>
    protected bool useSceling;
    /// <summary>
    /// масштабирование параметров воздействие
    /// </summary>
    protected int scale;
    /// <summary>
    /// Метод вычисления возращаемый параметров вызвавшему скил
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public ParametersStruct SolveSkillReturn(int level)
    {
        ParametersStruct tmp;
        if (useSceling)
            tmp = SkillParameters.Scale(scale);
        else
            tmp = ParametersStruct.Dummy();
        tmp.Assign(SkillReturnParameters.Scale(100 + 20 * level), returnMask);
        return tmp;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
