﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Общий абстрактный класс для скилов призыва и отзыва юнитов
/// </summary>
public abstract class SkillSummoning : SkillBase
{
    /// <summary>
    /// Получить класс призываемого существа
    /// </summary>
    /// <returns></returns>
    public abstract Type GetSummoningUnitClass();
    /// <summary>
    /// Получить призываемого юнита
    /// </summary>
    /// <returns></returns>
    public abstract BattleUnitBase GetSummoningUnit();
    /// <summary>
    /// Является ли этот скил призывающим
    /// </summary>
    /// <returns></returns>
    public abstract bool IsSummoningSkill();
    /// <summary>
    /// Является ли этот скил сменой юнита (на пример, оборотень)
    /// </summary>
    /// <returns></returns>
    public abstract bool IsTransformationSkill();
}

/// <summary>
/// Базовый класс для призыва существ
/// </summary>
/// <typeparam name="T"></typeparam>
public class SkillSummoning<T> : SkillSummoning where T : BattleUnitBase, new()
{
    /// <summary>
    /// Призванный юнит
    /// </summary>
    public T Unit { get { return new T(); } }

    public SkillSummoning()
    { }
    /// <summary>
    /// Получить класс призываемого существа
    /// </summary>
    /// <returns></returns>
    public override Type GetSummoningUnitClass()
    {
        return typeof(T);
    }
    /// <summary>
    /// Является ли этот скил сменой юнита (на пример, оборотень)
    /// </summary>
    /// <returns></returns>
    public override bool IsSummoningSkill()
    {
        return true;
    }
    /// <summary>
    /// Получить призываемого юнита
    /// </summary>
    /// <returns></returns>
    public override BattleUnitBase GetSummoningUnit()
    {
        var unit = Unit;
        unit.MarkAsSummoning();
        return unit;
    }
    /// <summary>
    /// Является ли этот скил сменой юнита (на пример, оборотень)
    /// </summary>
    /// <returns></returns>
    public override bool IsTransformationSkill()
    {
        return false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
/// <summary>
/// Скил отзыва призванного юнита
/// </summary>
/// <typeparam name="T"></typeparam>
public class SkillUnSummoning<T> : SkillSummoning where T : BattleUnitBase
{
    /// <summary>
    /// Класс отзываемых существ существа
    /// </summary>
    /// <returns></returns>
    public Type UnitType { get { return typeof(T); } }
    /// <summary>
    /// Получить класс отзываемых юнитов
    /// </summary>
    /// <returns></returns>
    public override Type GetSummoningUnitClass()
    {
        return UnitType;
    }
    /// <summary>
    /// Является ли этот скил призываюшим
    /// </summary>
    /// <returns></returns>
    public override bool IsSummoningSkill()
    {
        return false;
    }
    /// <summary>
    /// Получить призываемый юнит. Возращает NULL
    /// </summary>
    /// <returns></returns>
    public override BattleUnitBase GetSummoningUnit()
    {
        return null;
    }
    /// <summary>
    /// Конструктор
    /// </summary>
    public SkillUnSummoning()
    { }

    /// <summary>
    /// Является ли скилом трасформации (превращения)
    /// </summary>
    /// <returns></returns>
    public override bool IsTransformationSkill()
    {
        return false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
