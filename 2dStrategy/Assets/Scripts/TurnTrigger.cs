﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts;

/// <summary>
/// Класс триггера начала нового хода
/// </summary>
public class TurnTrigger : MonoBehaviour
{
    /// <summary>
    /// делегат обработчика события начала нового хода
    /// </summary>
    public event EventHandler<TriggerEvent> Triggered;
    /// <summary>
    /// Вызов обработчиков события начала нового хода
    /// </summary>
    /// <param name="e"></param>
    private void OnTriggered(TriggerEvent e)
    {
        EventHandler<TriggerEvent> handler = Triggered;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
