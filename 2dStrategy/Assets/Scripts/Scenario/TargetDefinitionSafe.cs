﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс задач по обороне зланий
    /// </summary>
    public class TargetDefinitionSafe : TargetDefinitionBase
    {
        /// <summary>
        /// Контейнер идентификаторов зданий для обороны
        /// </summary>
        public List<int> TargetBuildingsIds { get; set; }
    }
}
