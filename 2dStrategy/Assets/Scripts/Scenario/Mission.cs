﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для миссий
    /// </summary>
    public class Mission : GameTaskBase
    {
        protected List<TargetChain> targets;
        /// <summary>
        /// Контейнер задач миссии
        /// </summary>
        public List<TargetChain> Targets { get { return targets; } }
        /// <summary>
        /// Главные задачи
        /// </summary>
        public List<TargetChain> MainTargets { get { return GetMainTargets(); } }
        /// <summary>
        /// Побочные задачи
        /// </summary>
        public List<TargetChain> ExtTargets { get { return GetExtTargets(); } }
        /// <summary>
        /// Получить контейнер главных задач
        /// </summary>
        /// <returns></returns>
        private List<TargetChain> GetMainTargets()
        {
            var tx = from tc in targets
                     where tc.Targets.Any(t => t.IsCritical)
                     select tc;
            return tx.ToList();
        }
        /// <summary>
        /// Получить контейнер побочных задач
        /// </summary>
        /// <returns></returns>
        private List<TargetChain> GetExtTargets()
        {
            var tx = from tc in targets
                     where tc.Targets.All(t => !t.IsCritical)
                     select tc;
            return tx.ToList();
        }
        /// <summary>
        /// Инициализация обработчиков событий для задач миссии
        /// </summary>
        public void InitEventHandler()
        {
            foreach (var tc in targets)
                foreach (var t in tc.Targets)
                    t.TargetComplete += TargetCompleteHandler;
        }
        /// <summary>
        /// Обработчик события завершения задачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TargetCompleteHandler(object sender, TargetCompleteEventArgs e)
        {
            if (e.CompleteCode > 0)
            {
                TargetCompleteEventArgs missionComplete = new TargetCompleteEventArgs();
                missionComplete.CompleteCode = e.CompleteCode;
                missionComplete.CompleteState = TaskStateEnum.done;
                OnMissionComplete(missionComplete);
            }
            else if (e.CompleteState == TaskStateEnum.fail)
            {
                TargetCompleteEventArgs missionComplete = new TargetCompleteEventArgs();
                missionComplete.CompleteCode = e.CompleteCode;
                missionComplete.CompleteState = TaskStateEnum.fail;
                OnMissionComplete(missionComplete);
            }
            else
            {
                List<bool> mainTargetsComplete = new List<bool>();
                foreach (var tc in MainTargets)
                {
                    if (!tc.Targets.All(t => t.State == TaskStateEnum.done))
                        mainTargetsComplete.Add(false);
                    else
                        mainTargetsComplete.Add(true);
                }
                if (mainTargetsComplete.All(t => t == true))
                {
                    TargetCompleteEventArgs missionComplete = new TargetCompleteEventArgs();
                    missionComplete.CompleteCode = e.CompleteCode;
                    missionComplete.CompleteState = TaskStateEnum.done;
                    OnMissionComplete(missionComplete);
                }
            }
        }
        /// <summary>
        /// Обработчик завершения миссии
        /// </summary>
        public event EventHandler<TargetCompleteEventArgs> MissionComplete;
        /// <summary>
        /// Вызов обработчика события завершения миссии
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMissionComplete(TargetCompleteEventArgs e)
        {
            EventHandler<TargetCompleteEventArgs> handler = MissionComplete;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
