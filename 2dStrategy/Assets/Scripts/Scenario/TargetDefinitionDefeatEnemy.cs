﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач уничтожения героев противника
    /// </summary>
    public class TargetDefinitionDefeatEnemy : TargetDefinitionCrushKillDestroy
    {
        /// <summary>
        /// Отсутствуют цели-здания
        /// </summary>
        public override List<int> TargetBuildingsIds { get { return null; } }
        /// <summary>
        /// Отсутствуют цели-фракции
        /// </summary>
        public override List<FractionEnum> TargetFractions { get { return null; } }
    }
}
