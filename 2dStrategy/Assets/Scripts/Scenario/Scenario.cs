﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для сценария из миссий
    /// </summary>
    public class Scenario : GameTaskBase
    {
        /// <summary>
        /// список миссий сценария
        /// </summary>
        protected List<Mission> missions;

        /// <summary>
        /// Свойство. Список миссий сценария
        /// </summary>
        public List<Mission> Missions { get { return missions; } }
        /// <summary>
        /// Главные миссии
        /// </summary>
        public List<Mission> MainMissions { get { return GetMainMissions(); } }
        /// <summary>
        /// Побочные миссии
        /// </summary>
        public List<Mission> ExtMissions { get { return GetExtMissions(); } }
        /// <summary>
        /// Получить список главных миссий
        /// </summary>
        /// <returns></returns>
        private List<Mission> GetMainMissions()
        {
            var mx = from m in missions
                     where m.IsCritical
                     select m;
            return mx.ToList();
        }
        /// <summary>
        /// Получить список побочных миссий
        /// </summary>
        /// <returns></returns>
        private List<Mission> GetExtMissions()
        {
            var mx = from m in missions
                     where !m.IsCritical
                     select m;
            return mx.ToList();
        }
        /// <summary>
        /// Инициализация обработчико событий завершения миссий
        /// </summary>
        public void InitEventHandler()
        {
            foreach (var m in missions)
            {
                m.MissionComplete += MissionCompleteHandler;
                m.InitEventHandler();
            }
        }
        /// <summary>
        /// Обработчик события завершения миссии
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MissionCompleteHandler(object sender, TargetCompleteEventArgs e)
        {
            if (e.CompleteCode > 0)
            {
                TargetCompleteEventArgs scenarioComplete = new TargetCompleteEventArgs();
                scenarioComplete.CompleteCode = e.CompleteCode;
                scenarioComplete.CompleteState = TaskStateEnum.done;
                OnScenarioComplete(scenarioComplete);
            }
            else if (e.CompleteState == TaskStateEnum.fail)
            {
                TargetCompleteEventArgs scenarioComplete = new TargetCompleteEventArgs();
                scenarioComplete.CompleteCode = e.CompleteCode;
                scenarioComplete.CompleteState = TaskStateEnum.fail;
                OnScenarioComplete(scenarioComplete);
            }
            else
            {
                if (!MainMissions.All(t => t.State == TaskStateEnum.done))
                {
                    TargetCompleteEventArgs scenarioComplete = new TargetCompleteEventArgs();
                    scenarioComplete.CompleteCode = e.CompleteCode;
                    scenarioComplete.CompleteState = TaskStateEnum.done;
                    OnScenarioComplete(scenarioComplete);
                }
            }
        }
        /// <summary>
        /// делегат обработчика событий завершения сценария
        /// </summary>
        public event EventHandler<TargetCompleteEventArgs> ScenarioComplete;
        /// <summary>
        /// Вызов обработчика события завершения сценария
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnScenarioComplete(TargetCompleteEventArgs e)
        {
            EventHandler<TargetCompleteEventArgs> handler = ScenarioComplete;
            if (handler != null)
            {
                handler(this, e);
            }
        }

    }
}
