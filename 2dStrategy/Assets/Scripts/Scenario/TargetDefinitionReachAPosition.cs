﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс задач для достижения заданной позиции (экземпляра класса зданий)
    /// </summary>
    public class TargetDefinitionReachAPosition : TargetDefinitionBase
    {
        /// <summary>
        /// Идентификатор целевого здания
        /// </summary>
        public int TargetBuildingsId { get; set; }
    }
}
