﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Класс для смены задач
    /// </summary>
    public class TargetDefinitionToggle : TargetDefinitionBase
    {
        /// <summary>
        /// Текущая задача
        /// </summary>
        public TargetDefinitionBase ActualTarget { get; set; }
        /// <summary>
        /// Контейнер вариантов задач для смены в соответствии с игровым выбором
        /// </summary>
        public List<TargetDefinitionBase> PossibleTargetToggleList { get; set; }
    }
}
