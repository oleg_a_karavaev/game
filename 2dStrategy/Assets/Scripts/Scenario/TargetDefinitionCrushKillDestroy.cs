﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач уничтожения фракции, разрушения зданий фракции и уничтожения героев противника
    /// </summary>
    public class TargetDefinitionCrushKillDestroy : TargetDefinitionBase
    {
        /// <summary>
        /// Контейнер идентификаторов зданий для разрушения
        /// </summary>
        public virtual List<int> TargetBuildingsIds { get; set; }
        /// <summary>
        /// Идентификаторы героев противника для уничтожения
        /// </summary>
        public virtual List<int> TargetUnitsIds { get; set; }
        /// <summary>
        /// Идентификаторы фракций для уничтожения
        /// </summary>
        public virtual List<FractionEnum> TargetFractions { get; set; }
    }
}
