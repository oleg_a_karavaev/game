﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач миссий
    /// </summary>
    public class Target : GameTaskBase
    {
        protected TargetDefinitionBase targetDefinition;
        /// <summary>
        /// Определение задачи
        /// </summary>
        public TargetDefinitionBase TargetDefinition { get { return targetDefinition; } }
        /// <summary>
        /// Обработчик события завершения задачи
        /// </summary>
        public event EventHandler<TargetCompleteEventArgs> TargetComplete;
        /// <summary>
        /// Вызов обработчика события завершения задачи
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnTargetComplete(TargetCompleteEventArgs e)
        {
            EventHandler<TargetCompleteEventArgs> handler = TargetComplete;
            if (handler != null)
            {
                handler(this, e);
            }
        }

    }
    /// <summary>
    /// Класс события завершения задачи
    /// </summary>
    public class TargetCompleteEventArgs : EventArgs
    {
        public int CompleteCode { get; set; }
        public TaskStateEnum CompleteState { get; set; }
    }

}
