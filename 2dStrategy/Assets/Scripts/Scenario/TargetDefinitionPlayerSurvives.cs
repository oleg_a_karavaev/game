﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач выживания заданных юнитов
    /// </summary>
    public class TargetDefinitionPlayerSurvives : TargetDefinitionBase
    {
        /// <summary>
        /// Контейнер идентификаторов юнитов, которые должны выжить
        /// </summary>
        public List<int> TargetSurvivesIds { get; set; }
    }
}
