﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для уничтожения зданий противника
    /// </summary>
    public class TargetDefinitionDestroy : TargetDefinitionCrushKillDestroy
    {
        /// <summary>
        /// Отсутствуют цели-герои противника
        /// </summary>
        public override List<int> TargetUnitsIds { get { return null; } }
        /// <summary>
        /// отсутствуют цели фракции
        /// </summary>
        public override List<FractionEnum> TargetFractions { get { return null; } }
    }
}
