﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач по сбору ништяков
    /// </summary>
    public class TargetDefinitionGotAGoods : TargetDefinitionBase
    {
        /// <summary>
        /// Контейнер целевых ништяков: идентификатор, количество
        /// </summary>
        public List<KeyValuePair<int, int>> TargetGoodsId { get; set; }
    }
}
