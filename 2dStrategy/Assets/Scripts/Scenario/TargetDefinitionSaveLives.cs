﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач по защите фракций
    /// </summary>
    public class TargetDefinitionSaveLives : TargetDefinitionBase
    {
        /// <summary>
        /// Контейнер фракций, которые должны выжить
        /// </summary>
        public List<FractionEnum> TargetFractions { get; set; }
    }
}
