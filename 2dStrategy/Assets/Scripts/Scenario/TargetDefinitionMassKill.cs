﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Базовый класс для задач уничтожения фракций
    /// </summary>
    public class TargetDefinitionMassKill : TargetDefinitionCrushKillDestroy
    {
        /// <summary>
        /// отсутствуют цели-здания
        /// </summary>
        public override List<int> TargetBuildingsIds { get { return null; } }
        /// <summary>
        /// Отсутствуют цели-герои противника
        /// </summary>
        public override List<int> TargetUnitsIds { get { return null; } }
    }
}
