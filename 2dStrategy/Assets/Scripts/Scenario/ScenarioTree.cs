﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Класс для дерева сценариев (на текущей карте, или во всей игре)
    /// </summary>
    public class ScenarioTree
    {
        /*
         *  Каждый /узел содержит активный сценарий и доступные ветвления со своими сценариями. После завершения сценария предполагается возвращение кода завершения,
         *  который является указателем/индексом ветки для выбора следующей ветки сценариев. Если при этом веток больше нет - дерево считается завершённым
        */
        /// <summary>
        /// узлы
        /// </summary>
        private Dictionary<int, ScenarioTree> nodes;
        /// <summary>
        /// Свойство. Узлы/ветки дерева сценариев
        /// </summary>
        public Dictionary<int, ScenarioTree> Nodes { get { return nodes; } }

        /// <summary>
        /// Текущий активный сценарий
        /// </summary>
        private Scenario currentScenario;
        /// <summary>
        /// Свойство. Текущий активный сценарий
        /// </summary>
        public Scenario CurrentScenario { get { return currentScenario; } }
        /// <summary>
        /// Инициализация обработчиков события завершения сцценария
        /// </summary>
        public void InitEventHandler()
        {
            currentScenario.ScenarioComplete += ScenarioCompleteHandler;
            currentScenario.InitEventHandler();
        }
        /// <summary>
        /// Обработчик события завершения сценария
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ScenarioCompleteHandler(object sender, TargetCompleteEventArgs e)
        {
            if (e.CompleteState == TaskStateEnum.done)
            {
                if (Nodes.Count > 0) //Смена сценария, движение по дереву
                {
                    currentScenario = Nodes[e.CompleteCode].CurrentScenario;
                }
                else
                {
                    TargetCompleteEventArgs scenarioTreeComplete = new TargetCompleteEventArgs();
                    scenarioTreeComplete.CompleteCode = e.CompleteCode;
                    scenarioTreeComplete.CompleteState = TaskStateEnum.done;
                    OnScenarioTreeComplete(scenarioTreeComplete);
                }
            }
            else if (e.CompleteState == TaskStateEnum.fail)
            {
                TargetCompleteEventArgs scenarioTreeComplete = new TargetCompleteEventArgs();
                scenarioTreeComplete.CompleteCode = e.CompleteCode;
                scenarioTreeComplete.CompleteState = TaskStateEnum.fail;
                OnScenarioTreeComplete(scenarioTreeComplete);
            }
        }

        /// <summary>
        /// делегат обработчиков событий завершения дерева сценариев
        /// </summary>
        public event EventHandler<TargetCompleteEventArgs> ScenarioTreeComplete;
        /// <summary>
        /// Вызов обработчика события завершения дерева сценариев
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnScenarioTreeComplete(TargetCompleteEventArgs e)
        {
            EventHandler<TargetCompleteEventArgs> handler = ScenarioTreeComplete;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
