﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Перечислимый тип для состояния задачи/миссии/сценария
    /// </summary>
    public enum TaskStateEnum { disabled, activated, done, fail, locked };
    /// <summary>
    /// Базовый класс для игровых задач, миссий и сценариев
    /// </summary>
    public class GameTaskBase
    {
        /// <summary>
        /// Критичность задачи для прохождения. true - основная миссия, задача, сценарий; false - второстепенная
        /// </summary>
        public bool IsCritical { get; set; }

        /// <summary>
        /// Описание задачи
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Текущее состояние задачи
        /// </summary>
        public TaskStateEnum State { get; set; }
    }
}
