﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Scenario
{
    /// <summary>
    /// Класс последовательности задач
    /// </summary>
    public class TargetChain
    {
        /// <summary>
        /// Список задач
        /// </summary>
        private List<Target> targets;
        /// <summary>
        /// Список задач. Получение
        /// </summary>
        public List<Target> Targets { get { return targets; } }
    }
}
