﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс глобального состояния игры и сценариев
/// </summary>
public class GlobalScenarioState : MonoBehaviour
{
    /// <summary>
    /// последний полученный иденттификатор скила
    /// </summary>
    private static int skillId;
    /// <summary>
    /// Получить идентификатор скила
    /// </summary>
    /// <returns></returns>
    public static int GetSkillId()
    {
        return skillId++;
    }
    /// <summary>
    /// активные скилы
    /// </summary>
    public static Dictionary<int, SkillActive> activeSkills = new Dictionary<int, SkillActive>();
    /// <summary>
    /// активные скилы с возратом
    /// </summary>
    public static Dictionary<int, SkillActiveReturn> activeReturnSkills = new Dictionary<int, SkillActiveReturn>();
    /// <summary>
    /// скилы призыва/отзыва и превращения
    /// </summary>
    public static Dictionary<int, SkillSummoning> summoningSkills = new Dictionary<int, SkillSummoning>();
    /// <summary>
    /// скилы аур
    /// </summary>
    public static Dictionary<int, SkillAura> auraSkills = new Dictionary<int, SkillAura>();
    /// <summary>
    /// пассивные скилы
    /// </summary>
    public static Dictionary<int, SkillPassive> passiveSkills = new Dictionary<int, SkillPassive>();
    /// <summary>
    /// Скилы модификации состояния
    /// </summary>
    public static Dictionary<int, SkillStateModification> stateModificationSkills = new Dictionary<int, SkillStateModification>();
    /// <summary>
    /// Триггер начала нового хода
    /// </summary>
    public static TurnTrigger turnTrigger = new TurnTrigger();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
