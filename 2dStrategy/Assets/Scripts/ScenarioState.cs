﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс состояния текущего сценария
/// </summary>
public class ScenarioState : MonoBehaviour
{
    /// <summary>
    /// Доступные в сценарии юниты
    /// </summary>
    protected static Dictionary<FractionEnum, List<UnitBase>> unitDictionary = new Dictionary<FractionEnum, List<UnitBase>>();

    /// <summary>
    /// Добавить юнит в игру/сценарий
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    public static bool AddUnit(UnitBase unit)
    {
        if (!unitDictionary.ContainsKey(unit.Fraction))
        {
            unitDictionary.Add(unit.Fraction, new List<UnitBase>() { unit });
            return true;
        }
        else
        {
            if (unitDictionary[unit.Fraction].Exists(x => x.Id == unit.Id))
                return false;
            unitDictionary[unit.Fraction].Add(unit);
            return true;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
