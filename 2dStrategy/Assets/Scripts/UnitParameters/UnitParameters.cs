﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Класс параметров юнита
/// </summary>
public class UnitParameters : Object
{
    protected int level;
    protected int expLevel;
    protected UnitBattleParameters battleParameters;
    /// <summary>
    /// Боевые параметры юнита
    /// </summary>
    public UnitBattleParameters BattleParameters { get { return battleParameters; } }

    protected LevelSchemeEnum levelScheme;
    /// <summary>
    /// Класс юнита
    /// </summary>
    public LevelSchemeEnum LevelScheme { get { return levelScheme; } }

    /// <summary>
    /// веса классов и веток юнитов
    /// </summary>
    protected Dictionary<LevelSchemeEnum, uint> availableLevelSchemes = new Dictionary<LevelSchemeEnum, uint>(); // схема, вес (уровень схемы)

    /// <summary>
    /// Метод проверки доступности следующей ветки для класса юнита. Должен проверять порог уровня юнита
    /// </summary>
    /// <returns></returns>
    private bool NextLevelSchemeAvailabe()
    {
        return false;
    }

    public UnitParameters(int level, int expLevel, LevelSchemeEnum levelScheme)
    {
        battleParameters = new UnitBattleParameters(levelScheme);
        this.level = level;
        this.expLevel = expLevel;
    }

    /// <summary>
    /// Применить маску к боевым параметрам
    /// </summary>
    /// <param name="mask"></param>
    public void ApplyMask(int mask)
    {
        battleParameters.Parameters.Assign(BattleParameters.Parameters, mask);
    }
    /// <summary>
    /// Назначить боевые парарметры по маске
    /// </summary>
    /// <param name="param"></param>
    /// <param name="mask"></param>
    public void AssignMasked(UnitParameters param, int mask)
    {
        battleParameters.Parameters.Assign(param.BattleParameters.Parameters, mask);
    }
    /// <summary>
    /// Установить масштаб для боевых параметров
    /// </summary>
    /// <param name="val"></param>
    public void SetScale(int val)
    {
        battleParameters.SetScale(val);
    }
    /// <summary>
    /// Метод повышения уровня юнита и переключения класса
    /// </summary>
    private void ModifyLevelUp()
    {
        level++;
        bool isNextLevelSchemeAvailabe = NextLevelSchemeAvailabe();
        if (isNextLevelSchemeAvailabe)
        {
            /*должно быть обращение к гую и ответ с выбором новой линейки развития */
            LevelSchemeEnum newScheme = LevelScheme; // заглушка

            uint tgtWeight = availableLevelSchemes[newScheme];
            var needToRemove = from schW in availableLevelSchemes
                               where schW.Value <= tgtWeight
                               select schW.Key;
            foreach (var sch in needToRemove)
                availableLevelSchemes.Remove(sch);
            levelScheme = newScheme; // когда будет сделано получение результата выбора, то newScheme скорее всего не будет
        }
        battleParameters.ModifyLevelUp(levelScheme, isNextLevelSchemeAvailabe);
    }
    /// <summary>
    /// Назначить бонус к боевым параметрам
    /// </summary>
    /// <param name="bonus"></param>
    public void AssignParametersBonus(UnitBattleParameters bonus)
    {
        battleParameters.AssignParametersBonus(bonus);
    }

}
