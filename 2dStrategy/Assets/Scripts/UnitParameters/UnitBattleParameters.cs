﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Перечислимы тип вида атаки
/// </summary>
public enum AttackTypeEnum { melee, archery, magic, notAnalize };

/// <summary>
/// Структура параметров магических атак и/или защиты
/// </summary>
public struct MagicAttackDefenseStruct
{
    /// <summary>
    /// огонь
    /// </summary>
    public int fire;
    /// <summary>
    /// лёд/холод
    /// </summary>
    public int cold;
    /// <summary>
    /// электричество/молния
    /// </summary>
    public int electricity;
    /// <summary>
    /// дух (вампиризм, лечение, усыпление, благословление, проклятие и т.п.)
    /// </summary>
    public int spirit;

    public MagicAttackDefenseStruct (int fire, int cold, int electricity, int spirit)
    {
        this.fire = fire;
        this.cold = cold;
        this.electricity = electricity;
        this.spirit = spirit;
    }

    /// <summary>
    /// добавить параметры дугой сруктуры
    /// </summary>
    /// <param name="attack"></param>
    public void Add(MagicAttackDefenseStruct attack)
    {
        this.fire += attack.fire;
        this.cold += attack.cold;
        this.electricity += attack.electricity;
        this.spirit += attack.spirit;
    }
    /// <summary>
    /// Изменить текущие параметры на процент, определяемый во входных параметрах
    /// </summary>
    /// <param name="attack"></param>
    public void AddMulFrac(MagicAttackDefenseStruct attack)
    {
        this.fire += (int)(this.fire * attack.fire / 100);
        this.cold += (int)(this.cold * attack.cold / 100);
        this.electricity += (int)(this.electricity * attack.electricity / 100);
        this.spirit += (int)(this.spirit * attack.spirit / 100);
    }
    /// <summary>
    /// Изменить текущие параметры на процент, определяемый входным параметром
    /// </summary>
    /// <param name="scale"></param>
    public void ApplyScale(int scale)
    {
        this.fire += (int)(this.fire * scale / 100);
        this.cold += (int)(this.cold * scale / 100);
        this.electricity += (int)(this.electricity * scale / 100);
        this.spirit += (int)(this.spirit * scale / 100);
    }
    /// <summary>
    /// Масштабирование параметров на заанный коэффициент
    /// </summary>
    /// <param name="scale"></param>
    /// <returns>Параметры магической атаки/защиты</returns>
    public MagicAttackDefenseStruct Scale(int scale)
    {
        return new MagicAttackDefenseStruct(
              (int)(this.fire * scale / 100)
            , (int)(this.cold * scale / 100)
            , (int)(this.electricity * scale / 100)
            , (int)(this.spirit * scale / 100)
        );
    }
}
/// <summary>
/// Структура пармаетров физической атаки
/// </summary>
public struct PhysicalAttackStruct
{
    /// <summary>
    /// дробящий урон
    /// </summary>
    public int crushing;
    /// <summary>
    /// пробивание брони
    /// </summary>
    public int armorPenetration;
    /// <summary>
    /// обычная атака
    /// </summary>
    public int normalAttack;
    /// <summary>
    /// яд
    /// </summary>
    public int poison;

    public PhysicalAttackStruct (int crushing, int armorPenetration, int normalAttack, int poison)
    {
        this.crushing = crushing;
        this.armorPenetration = armorPenetration;
        this.normalAttack = normalAttack;
        this.poison = poison;
    }
    /// <summary>
    /// добавить параметры дугой сруктуры
    /// </summary>
    /// <param name="attack"></param>
    public void Add(PhysicalAttackStruct attack)
    {
        this.crushing += attack.crushing;
        this.armorPenetration += attack.armorPenetration;
        this.normalAttack += attack.normalAttack;
        this.poison += attack.poison;
    }
    /// <summary>
    /// Изменить текущие параметры на процент, определяемый во входных параметрах
    /// </summary>
    /// <param name="attack"></param>
    public void AddMulFrac(PhysicalAttackStruct attack)
    {
        this.crushing += (int)(this.crushing * attack.crushing / 100);
        this.armorPenetration += (int)(this.armorPenetration * attack.armorPenetration / 100);
        this.normalAttack += (int)(this.normalAttack * attack.normalAttack / 100);
        this.poison += (int)(this.poison * attack.poison / 100);
    }
    /// <summary>
    /// Масштабировать текущие параметры значениями, определяемыми входным параметром
    /// </summary>
    /// <param name="scale"></param>
    public void ApplyScale(int scale)
    {
        this.crushing += (int)(this.crushing * scale / 100);
        this.armorPenetration += (int)(this.armorPenetration * scale / 100);
        this.normalAttack += (int)(this.normalAttack * scale / 100);
        this.poison += (int)(this.poison * scale / 100);
    }
    /// <summary>
    /// Получить масштабированное значение текущих параметров на заданныйй коэффициент
    /// </summary>
    /// <param name="scale"></param>
    /// <returns>Параметры физической атаки</returns>
    public PhysicalAttackStruct Scale(int scale)
    {
        return new PhysicalAttackStruct(
              (int)(this.crushing * scale / 100)
            , (int)(this.armorPenetration * scale / 100)
            , (int)(this.normalAttack * scale / 100)
            , (int)(this.poison * scale / 100)
        );
    }
}

/// <summary>
/// Перечислимы тип классов юнита
/// </summary>
[Flags] public enum LevelSchemeEnum : uint
{
      magician_base = 0x00000001
    , magician_battle = 0x00000002
    , magician_fire_of_Veles = 0x00000004 // Огонь Велеса. Магия огня (фаэрболы, и т.п.) + меч с горящим клинком
    , magician_support = 0x00000008
    , magician_palm_of_Makosh = 0x00000010 // Длань Макоши. Исцеление, снатие заклятий, проклятий и т.п.
    , magician_palm_of_Morena = 0x00000020 // Длань Морены. Воскрешение, смертельное проклятие, возможно поднятие скелетов и их разрушение
    , melee_base = 0x00000040
    , melee_spearman = 0x00000080
    , melee_spear_of_Gods = 0x00000100 // Копьё богов. Убойное пробивание брони любой природы. Всегда высокий боевой дух. Всегда отвечает, не отступает
    , melee_2hand = 0x00000200
    , melee_poleaxe_of_Veles = 0x00000400 // Секира Велеса (если память не изменяет, у него серебрянная секира). Здесь - кто-то вроде паладина
    , melee_1hand = 0x00000800
    , melee_soldier_of_Perun = 0x00001000 // Перуна солдат. Мечник или с молотом. При ударе наносит урон электриеством (молния)
    , melee_double = 0x00002000
    , melee_rage_of_Perun = 0x00004000 // Ярость Перуна. Оружие в обеих руках. Огромный урон, малая защита, много жизней, большая скорость, повышена защита от магии
    , archer_base = 0x00008000
    , archer_scout = 0x00010000
    , archer_eye_of_Veles = 0x00020000 // Глаз Велеса. 100% обнаружение засады, умеет в ближний бой, хорошая защита от магии, дополнительный урон магам, высокая вероятность критического удара. Улучшает проходимость отряда по любой местность
    , archer_heavy = 0x00040000
    , archer_reaper_of_Fate = 0x00080000 // Жнец рока. Уязвим для магии, высокая физическая защита, Нет штрафа дальности, двойной выстрел, пробивание брони
}
/// <summary>
/// Структура масок параметров
/// </summary>
public struct ParameterMask
{
    public const int magicAttack = 0x00000001;
    public const int magicDefense = 0x00000002;
    public const int physicalAttack = 0x00000004;
    public const int physycalDefense = 0x00000008;
    public const int immunityToPoison = 0x00000010;
    public const int crushDefense = 0x00000020;
    public const int archeryDefense = 0x00000040;
    public const int armor = 0x00000080;
    public const int physicalAttackType = 0x00000100;
    public const int initiative = 0x00000200;
    public const int fightingSpirit = 0x00000400;
    public const int helth = 0x00000800;
    public const int speed = 0x00001000;
    public const int demage = 0x00002000;
    public const int manna = 0x00004000;

    /// <summary>
    /// Выбор всех параметров
    /// </summary>
    /// <returns></returns>
    public static int FullMask ()
    {
        return
              magicAttack
            | magicDefense
            | physicalAttack
            | physycalDefense
            | immunityToPoison
            | crushDefense
            | archeryDefense
            | armor
            | physicalAttackType
            | initiative
            | fightingSpirit
            | helth
            | speed
            | demage
            | manna;
    }
    /// <summary>
    /// Выбор параметров, кроме атак
    /// </summary>
    /// <returns></returns>
    public static int NoAttackMask()
    {
        return
              magicDefense
            | physycalDefense
            | immunityToPoison
            | crushDefense
            | archeryDefense
            | armor
            | initiative
            | fightingSpirit
            | helth
            | speed
            | manna;
    }
}


/// <summary>
/// Структура параметров
/// </summary>
public struct ParametersStruct
{
    /// <summary>
    /// Волшебная атака
    /// </summary>
    public MagicAttackDefenseStruct magicAttack;
    /// <summary>
    /// Защита от магии
    /// </summary>
    public MagicAttackDefenseStruct magicDefense;
    /// <summary>
    /// Физическая атака
    /// </summary>
    public PhysicalAttackStruct physicalAttack;
    /// <summary>
    /// Физическая защита
    /// </summary>
    public int physycalDefense;
    /// <summary>
    /// Иммунитет к ядам
    /// </summary>
    public int immunityToPoison;
    /// <summary>
    /// Защита от дробящего урона
    /// </summary>
    public int crushDefense;
    /// <summary>
    /// Защита от стрел
    /// </summary>
    public int archeryDefense;
    /// <summary>
    /// Броня
    /// </summary>
    public int armor;
    /// <summary>
    /// Тип атаки
    /// </summary>
    public AttackTypeEnum attackType;
    /// <summary>
    /// инициатива
    /// </summary>
    public int initiative;
    /// <summary>
    /// Боевой дух
    /// </summary>
    public int fightingSpirit;
    /// <summary>
    /// максимальное здоровье
    /// </summary>
    public int helth;
    /// <summary>
    /// скорость
    /// </summary>
    public int speed;
    /// <summary>
    /// урон
    /// </summary>
    public int demage;
    /// <summary>
    /// максимальная манна
    /// </summary>
    public int manna;

    public ParametersStruct(
          MagicAttackDefenseStruct magicAttack
        , MagicAttackDefenseStruct magicDefense
        , PhysicalAttackStruct physicalAttack
        , int physycalDefense
        , int immunityToPoison
        , int crushDefense
        , int archeryDefense
        , int armor
        , AttackTypeEnum attackType
        , int initiative
        , int fightingSpirit
        , int helth
        , int speed
        , int demage
        , int manna
    )
    {
        this.magicAttack = magicAttack;
        this.magicDefense = magicDefense;
        this.physicalAttack = physicalAttack;
        this.physycalDefense = physycalDefense;
        this.immunityToPoison = immunityToPoison;
        this.archeryDefense = archeryDefense;
        this.crushDefense = crushDefense;
        this.armor = armor;
        this.attackType = attackType;
        this.initiative = initiative;
        this.fightingSpirit = fightingSpirit;
        this.helth = helth;
        this.speed = speed;
        this.demage = demage;
        this.manna = manna;
    }
    /// <summary>
    /// "Пустышка", все параметры == 0
    /// </summary>
    /// <returns></returns>
    public static ParametersStruct Dummy()
    {
        ParametersStruct tmp = new ParametersStruct();
        tmp.magicAttack = new MagicAttackDefenseStruct(0, 0, 0, 0);
        tmp.magicDefense = new MagicAttackDefenseStruct(0, 0, 0, 0);
        tmp.physicalAttack = new PhysicalAttackStruct(0, 0, 0, 0);
        tmp.physycalDefense = 0;
        tmp.immunityToPoison = 0;
        tmp.archeryDefense = 0;
        tmp.crushDefense = 0;
        tmp.armor = 0;
        tmp.attackType = AttackTypeEnum.notAnalize;
        tmp.initiative = 0;
        tmp.fightingSpirit = 0;
        tmp.helth = 0;
        tmp.speed = 0;
        tmp.demage = 0;
        tmp.manna = 0;
        return tmp;
    }
    /// <summary>
    /// Добавление входных параметров
    /// </summary>
    /// <param name="addParam"></param>
    public void Apply(ParametersStruct addParam)
    {
        this.magicAttack.Add(addParam.magicAttack);
        this.magicDefense.Add(addParam.magicDefense);
        this.physicalAttack.Add(addParam.physicalAttack);
        this.physycalDefense += addParam.physycalDefense;
        this.immunityToPoison += addParam.immunityToPoison;
        this.archeryDefense += addParam.archeryDefense;
        this.crushDefense += addParam.crushDefense;
        this.armor += addParam.armor;
        this.initiative += addParam.initiative;
        this.fightingSpirit += addParam.fightingSpirit;
        this.helth += addParam.helth;
        this.speed += addParam.speed;
        this.demage += addParam.demage;
        this.manna += addParam.manna;
    }
    /// <summary>
    /// Добавление входных параметров по маске
    /// </summary>
    /// <param name="addParam"></param>
    /// <param name="mask"></param>
    public void Apply(ParametersStruct addParam, int mask)
    {
        if ((mask & ParameterMask.magicAttack) > 0)
            this.magicAttack.Add(addParam.magicAttack);
        if ((mask & ParameterMask.magicDefense) > 0)
            this.magicDefense.Add(addParam.magicDefense);
        if ((mask & ParameterMask.physicalAttack) > 0)
            this.physicalAttack.Add(addParam.physicalAttack);
        if ((mask & ParameterMask.physycalDefense) > 0)
            this.physycalDefense += addParam.physycalDefense;
        if ((mask & ParameterMask.immunityToPoison) > 0)
            this.immunityToPoison += addParam.immunityToPoison;
        if ((mask & ParameterMask.archeryDefense) > 0)
            this.archeryDefense += addParam.archeryDefense;
        if ((mask & ParameterMask.crushDefense) > 0)
            this.crushDefense += addParam.crushDefense;
        if ((mask & ParameterMask.armor) > 0)
            this.armor += addParam.armor;
        if ((mask & ParameterMask.initiative) > 0)
            this.initiative += addParam.initiative;
        if ((mask & ParameterMask.fightingSpirit) > 0)
            this.fightingSpirit += addParam.fightingSpirit;
        if ((mask & ParameterMask.helth) > 0)
            this.helth += addParam.helth;
        if ((mask & ParameterMask.speed) > 0)
            this.speed += addParam.speed;
        if ((mask & ParameterMask.demage) > 0)
            this.demage += addParam.demage;
        if ((mask & ParameterMask.manna) > 0)
            this.manna += addParam.manna;
    }
    /// <summary>
    /// Назначение параметров по маске
    /// </summary>
    /// <param name="addParam"></param>
    /// <param name="mask"></param>
    public void Assign(ParametersStruct addParam, int mask)
    {
        if ((mask & ParameterMask.magicAttack) > 0)
            this.magicAttack = addParam.magicAttack;
        if ((mask & ParameterMask.magicDefense) > 0)
            this.magicDefense = addParam.magicDefense;
        if ((mask & ParameterMask.physicalAttack) > 0)
            this.physicalAttack = addParam.physicalAttack;
        if ((mask & ParameterMask.physycalDefense) > 0)
            this.physycalDefense = addParam.physycalDefense;
        if ((mask & ParameterMask.immunityToPoison) > 0)
            this.immunityToPoison = addParam.immunityToPoison;
        if ((mask & ParameterMask.archeryDefense) > 0)
            this.archeryDefense = addParam.archeryDefense;
        if ((mask & ParameterMask.crushDefense) > 0)
            this.crushDefense = addParam.crushDefense;
        if ((mask & ParameterMask.armor) > 0)
            this.armor = addParam.armor;
        if ((mask & ParameterMask.initiative) > 0)
            this.initiative = addParam.initiative;
        if ((mask & ParameterMask.fightingSpirit) > 0)
            this.fightingSpirit = addParam.fightingSpirit;
        if ((mask & ParameterMask.helth) > 0)
            this.helth = addParam.helth;
        if ((mask & ParameterMask.speed) > 0)
            this.speed = addParam.speed;
        if ((mask & ParameterMask.demage) > 0)
            this.demage = addParam.demage;
        if ((mask & ParameterMask.physicalAttackType) > 0)
            this.attackType = addParam.attackType;
        if ((mask & ParameterMask.manna) > 0)
            this.manna = addParam.manna;
    }
    /// <summary>
    /// Получение суммы параметров по маске. Результат не применяется к значениям вызвавшего экземпляра
    /// </summary>
    /// <param name="addParam"></param>
    /// <param name="mask"></param>
    /// <returns></returns>
    public ParametersStruct Add(ParametersStruct addParam, int mask)
    {
        ParametersStruct tmp = this;
        tmp.Apply(addParam, mask);
        return tmp;
    }
    /// <summary>
    /// Получение суммы массива параметров по маске. Результат не применяется к значениям вызвавшего экземпляра
    /// </summary>
    /// <param name="addParam"></param>
    /// <param name="mask"></param>
    /// <returns></returns>
    public ParametersStruct Add(ParametersStruct[] addParams, int mask)
    {
        ParametersStruct tmp = this;
        foreach (var p in addParams)
            tmp.Apply(p, mask);
        return tmp;
    }

    /// <summary>
    /// Получение суммы массива параметров по маске. Результат не применяется к значениям вызвавшего экземпляра
    /// </summary>
    /// <param name="addParam">Массив пар: структура параметров, маска</param>
    /// <param name="mask"></param>
    /// <returns></returns>
    public ParametersStruct Add(KeyValuePair<ParametersStruct, int>[] addParams)
    {
        ParametersStruct tmp = this;
        if (addParams != null)
            foreach (var p in addParams)
                tmp.Apply(p.Key, p.Value);
        return tmp;
    }
    /// <summary>
    /// Изменить текущие параметры на процент, определяемый во входных параметрах
    /// </summary>
    /// <param name="attack"></param>
    public void ApplyMul(ParametersStruct mulParam)
    {
        this.magicAttack.AddMulFrac(mulParam.magicAttack);
        this.magicDefense.AddMulFrac(mulParam.magicDefense);
        this.physicalAttack.AddMulFrac(mulParam.physicalAttack);
        this.physycalDefense += (int)(this.physycalDefense * mulParam.physycalDefense / 100);
        this.immunityToPoison += (int)(this.immunityToPoison * mulParam.immunityToPoison / 100);
        this.archeryDefense += (int)(this.archeryDefense * mulParam.archeryDefense / 100);
        this.crushDefense += (int)(this.crushDefense * mulParam.crushDefense / 100);
        this.armor += (int)(this.armor * mulParam.armor / 100);
        this.initiative += (int)(this.initiative * mulParam.initiative / 100);
        this.fightingSpirit += (int)(this.fightingSpirit * mulParam.fightingSpirit / 100);
        this.helth += (int)(this.helth * mulParam.helth / 100);
        this.speed += (int)(this.speed * mulParam.speed / 100);
        this.demage += (int)(this.demage * mulParam.demage / 100);
        this.manna += (int)(this.manna * mulParam.manna / 100);
    }
    /// <summary>
    /// Масштабировать текущие параметры значениями, определяемыми входным параметром
    /// </summary>
    /// <param name="scale"></param>
    public void ApplyScale(int scale)
    {
        this.magicAttack.Scale(scale);
        this.magicDefense.Scale(scale);
        this.physicalAttack.Scale(scale);
        this.physycalDefense += (int)(this.physycalDefense * scale / 100);
        this.immunityToPoison += (int)(this.immunityToPoison * scale / 100);
        this.archeryDefense += (int)(this.archeryDefense * scale / 100);
        this.crushDefense += (int)(this.crushDefense * scale / 100);
        this.armor += (int)(this.armor * scale / 100);
        this.initiative += (int)(this.initiative * scale / 100);
        this.fightingSpirit += (int)(this.fightingSpirit * scale / 100);
        this.helth += (int)(this.helth * scale / 100);
        this.speed += (int)(this.speed * scale / 100);
        this.demage += (int)(this.demage * scale / 100);
        this.manna += (int)(this.manna * scale / 100);
    }
    /// <summary>
    /// Получить масштабированное значение текущих параметров на заданныйй коэффициент
    /// </summary>
    /// <param name="scale"></param>
    public ParametersStruct Scale(int scale)
    {
        return new ParametersStruct(
              this.magicAttack.Scale(scale)
            , this.magicDefense.Scale(scale)
            , this.physicalAttack.Scale(scale)
            , (int)(this.physycalDefense * scale / 100)
            , (int)(this.immunityToPoison * scale / 100)
            , (int)(this.archeryDefense * scale / 100)
            , (int)(this.crushDefense * scale / 100)
            , (int)(this.armor * scale / 100)
            , this.attackType
            , (int)(this.initiative * scale / 100)
            , (int)(this.fightingSpirit * scale / 100)
            , (int)(this.helth * scale / 100)
            , (int)(this.speed * scale / 100)
            , (int)(this.demage * scale / 100)
            , (int)(this.manna * scale / 100)
        );
    }
}

/// <summary>
/// Класс боевых параметров
/// </summary>
public class UnitBattleParameters
{
    /* структуры для процентного приращения параметров для разных базовых классов. При увеличении уровня */
    static private ParametersStruct magicianParamIncr = new ParametersStruct(new MagicAttackDefenseStruct(20,20,20,20), new MagicAttackDefenseStruct(20, 20, 20, 20), new PhysicalAttackStruct(5, 5, 5, 0), 5, 0, 5, 0, 5, AttackTypeEnum.notAnalize, 15, 0, 10, 0, 20, 20);
    static private ParametersStruct meleeParamIncr = new ParametersStruct(new MagicAttackDefenseStruct(5, 5, 5, 5), new MagicAttackDefenseStruct(10, 10, 10, 10), new PhysicalAttackStruct(20, 20, 20, 0), 20, 0, 10, 20, 10, AttackTypeEnum.notAnalize, 5, 10, 10, 0, 20, 20);
    static private ParametersStruct archerParamIncr = new ParametersStruct(new MagicAttackDefenseStruct(5, 5, 5, 5), new MagicAttackDefenseStruct(15, 15, 15, 15), new PhysicalAttackStruct(20, 20, 20, 0), 15, 0, 20, 10, 10, AttackTypeEnum.notAnalize, 10, 5, 10, 0, 20, 20);
    /* начальные параметры для разных классов юнитов */
    static private ParametersStruct magicianBaseParam = new ParametersStruct(new MagicAttackDefenseStruct(20, 20, 20, 20), new MagicAttackDefenseStruct(20, 20, 20, 20), new PhysicalAttackStruct(0,0,0, 0), 0, 10, 0, 0, 0, AttackTypeEnum.magic, 15, 10, 100, 5, 20, 100);
    static private ParametersStruct magicianBattleParam = new ParametersStruct(new MagicAttackDefenseStruct(40, 40, 40, 40), new MagicAttackDefenseStruct(40, 40, 40, 40), new PhysicalAttackStruct(20, 5, 10, 0), 20, 10, 10, 10, 10,  AttackTypeEnum.magic, 40, 20, 200, 7, 40, 200);
    static private ParametersStruct magicianFireOfVelesParam = new ParametersStruct(new MagicAttackDefenseStruct(60, 60, 60, 40), new MagicAttackDefenseStruct(60, 60, 60, 60), new PhysicalAttackStruct(40, 10, 15, 0), 40, 20, 20, 15, 20, AttackTypeEnum.magic, 40, 30, 400, 7, 60, 400);
    static private ParametersStruct magicianSupportParam = new ParametersStruct(new MagicAttackDefenseStruct(30, 30, 30, 50), new MagicAttackDefenseStruct(60, 60, 60, 60), new PhysicalAttackStruct(0, 0, 0, 0), 5, 40, 30, 10, 20, AttackTypeEnum.magic, 40, 20, 150, 4, 40, 200);
    static private ParametersStruct magicianPalmOfMakoshParam = new ParametersStruct(new MagicAttackDefenseStruct(30, 30, 30, 80), new MagicAttackDefenseStruct(80, 80, 80, 80), new PhysicalAttackStruct(0, 0, 0, 0), 10, 40, 40, 20, 25, AttackTypeEnum.magic, 80, 30, 200, 4, 80, 400);
    static private ParametersStruct magicianPalmOfMorenaParam = new ParametersStruct(new MagicAttackDefenseStruct(30, 60, 30, 90), new MagicAttackDefenseStruct(80, 80, 80, 80), new PhysicalAttackStruct(20, 20, 10, 0), 20, 60, 60, 25, 25, AttackTypeEnum.magic, 80, 30, 350, 4, 80, 400);
    static private ParametersStruct meleeBaseParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(5, 5, 5, 5), new PhysicalAttackStruct(20, 10, 10, 0), 20, 10, 20, 20, 20, AttackTypeEnum.melee, 10, 20, 200, 7, 20, 100);
    static private ParametersStruct meleeSpearmanParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(20, 20, 20, 20), new PhysicalAttackStruct(40, 10, 40, 0), 40, 10, 40, 40, 40, AttackTypeEnum.melee, 5, 40, 400, 5, 40, 150);
    static private ParametersStruct meleeSpearOfGodsParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 20), new MagicAttackDefenseStruct(80, 80, 80, 80), new PhysicalAttackStruct(80, 15, 80, 0), 80, 40, 80, 80, 80, AttackTypeEnum.melee, 15, 100, 600, 5, 80, 200);
    static private ParametersStruct melee2HandParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(30, 30, 30, 30), new PhysicalAttackStruct(60, 20, 20, 0), 20, 10, 30, 30, 30, AttackTypeEnum.melee, 15, 30, 10, 7, 60, 150);
    static private ParametersStruct meleePoleaxeOfVelesParam = new ParametersStruct(new MagicAttackDefenseStruct(40, 0, 0, 40), new MagicAttackDefenseStruct(40, 40, 40, 40), new PhysicalAttackStruct(80, 20, 40, 0), 60, 30, 60, 60, 60, AttackTypeEnum.melee, 15, 50, 500, 7, 80, 400);
    static private ParametersStruct melee1HandParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(20, 20, 20, 20), new PhysicalAttackStruct(50, 15, 15, 0), 30, 10, 50, 30, 50, AttackTypeEnum.melee, 15, 30, 350, 5, 50, 150);
    static private ParametersStruct meleeSoldierOfPerundParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 40, 40), new MagicAttackDefenseStruct(40, 40, 40, 40), new PhysicalAttackStruct(70, 70, 30, 0), 70, 30, 90, 70, 90, AttackTypeEnum.melee, 15, 70, 600, 7, 70, 200);
    static private ParametersStruct meleeDoubleParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(10, 10, 10, 10), new PhysicalAttackStruct(80, 25, 20, 0), 10, 10, 10, 50, 10, AttackTypeEnum.melee, 20, 30, 600, 0, 80, 150);
    static private ParametersStruct meleeRageOfPerunParam = new ParametersStruct(new MagicAttackDefenseStruct(5, 5, 5, 5), new MagicAttackDefenseStruct(15, 15, 15, 15), new PhysicalAttackStruct(120, 25, 30, 0), 15, 15, 20, 100, 15, AttackTypeEnum.melee, 30, 60, 1000, 10, 120, 200);
    static private ParametersStruct archerBaseParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(10, 10, 10, 10), new PhysicalAttackStruct(20, 5, 15, 0), 10, 10, 20, 20, 10, AttackTypeEnum.archery, 15, 10, 120, 5, 20, 100);
    static private ParametersStruct archerScoutParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(15, 15, 15, 15), new PhysicalAttackStruct(40, 15, 15, 40), 15, 40, 25, 25, 15, AttackTypeEnum.archery, 50, 30, 200, 6, 40, 200);
    static private ParametersStruct archerEyeOfVelesParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 30, 0, 30), new MagicAttackDefenseStruct(60, 60, 60, 60), new PhysicalAttackStruct(60, 30, 20, 70), 30, 80, 30, 30, 20, AttackTypeEnum.archery, 80, 50, 300, 7, 80, 400);
    static private ParametersStruct archerHeavyParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(10, 10, 10, 10), new PhysicalAttackStruct(60, 15, 40, 0), 30, 15, 30, 30, 20, AttackTypeEnum.archery, 30, 60, 200, 4, 60, 150);
    static private ParametersStruct archerReaperOfFateParam = new ParametersStruct(new MagicAttackDefenseStruct(0, 0, 0, 0), new MagicAttackDefenseStruct(15, 15, 15, 15), new PhysicalAttackStruct(120, 15, 60, 0), 80, 15, 80, 50, 40, AttackTypeEnum.archery, 45, 80, 500, 4, 120, 200);
    /// <summary>
    /// Начальные параметры для колдуна
    /// </summary>
    static public ParametersStruct MagicianBaseParam { get { return magicianBaseParam; } }
    /// <summary>
    /// Начальные параметры для боевого мага/ведьмака (ведьмак - это как в книге/сериале/игре про Геральта, но больше колдун, чем убивалка мностров и людей мечом)
    /// </summary>
    static public ParametersStruct MagicianBattleParam { get { return magicianBattleParam; } }
    /// <summary>
    /// Начальные параметры для топового боевого колдуна - Огонь Велеса
    /// </summary>
    static public ParametersStruct MagicianFireOfVelesParam { get { return magicianFireOfVelesParam; } }
    /// <summary>
    /// Параметры для мага поддержки (лечение, благословление, ...)
    /// </summary>
    static public ParametersStruct MagicianSupportParam { get { return magicianSupportParam; } }
    /// <summary>
    /// параметры для топового "светлого" мага поддержки - Длань Макоши
    /// Доступ к этому классу определяется выполнением определённых условий при прохождении
    /// Закрывает доступ к Длани Морены
    /// </summary>
    static public ParametersStruct MagicianPalmOfMakoshParam { get { return magicianPalmOfMakoshParam; } }
    /// <summary>
    /// Параметры для топового мага поддержки - Длань Морены. Это про жизнь-смерть.
    /// Поднятие скелетов, отзыв поднятых скелетов, воскрешение из мёртвых, смертельное проклятие, может дать пиздюлей
    /// Доступ к этому классу определяется выполнением определённых условий при прохождении
    /// Закрывает доступ к Длани Макоши и, возможно, Глазу Велеса, или Секире Велеса
    /// </summary>
    static public ParametersStruct MagicianPalmOfMorenaParam { get { return magicianPalmOfMorenaParam; } }
    /// <summary>
    /// Начальные параметры для ратника
    /// </summary>
    static public ParametersStruct MeleeBaseParam { get { return meleeBaseParam; } }
    /// <summary>
    /// Начальные параметры для копейщика (ветка от ратника)
    /// </summary>
    static public ParametersStruct MeleeSpearmanParam { get { return meleeSpearmanParam; } }
    /// <summary>
    /// Параметры топового копейщика - Копьё богов. Самое высокое пробивание брони в игре, самый высокий боевой дух, высокая броня, ни когда не отступает, всегда бьёт в ответ
    /// </summary>
    static public ParametersStruct MeleeSpearOfGodsParam { get { return meleeSpearOfGodsParam; } }
    /// <summary>
    /// Начальные параметры для воина с двуручным оружием (секира, молот, или меч), ветка от ратника
    /// </summary>
    static public ParametersStruct Melee2HandParam { get { return melee2HandParam; } }
    /// <summary>
    /// Начальные параметры для топового воина с двуручным оружием - Секира Велеса. Что-то вроде паладина.
    /// Если память не изменяет у Велеса серебряная секира (читал где-то такое), а раз серебряная - то пусть будет увеличенный урон нечисти.
    /// Можно ещё что-нибудь добавить: инициативы на пример
    /// </summary>
    static public ParametersStruct MeleePoleaxeOfVelesParam { get { return meleePoleaxeOfVelesParam; } }
    /// <summary>
    /// Начальные параметры воина с одноручным оружием и щитом (ветка от "ратника")
    /// </summary>
    static public ParametersStruct Melee1HandParam { get { return melee1HandParam; } }
    /// <summary>
    /// Начальные параметры топового воина с одноручным оружием - Воин Перуна. Правильное вооружение - молот, при ударе может ударить и молнией.
    /// Или ещё как-то добавить удар молнией.
    /// </summary>
    static public ParametersStruct MeleeSoldierOfPerundParam { get { return meleeSoldierOfPerundParam; } }
    /// <summary>
    /// Начальные параметры воина с оружием в двух руках (ветка от "ратника")
    /// </summary>
    static public ParametersStruct MeleeDoubleParam { get { return meleeDoubleParam; } }
    /// <summary>
    /// параметры топового воина с оружием в обеих руках - Ярость Перуна. Быстрый, много жизней, высокая инициатива, самый большой урон в игре
    /// низкая физическая защита. Тут надо добавлять функционал ограничения доступного классу снаряжения
    /// </summary>
    static public ParametersStruct MeleeRageOfPerunParam { get { return meleeRageOfPerunParam; } }
    /// <summary>
    /// Начальные параметры лучника
    /// </summary>
    static public ParametersStruct ArcherBaseParam { get { return archerBaseParam; } }
    /// <summary>
    /// Начальные параметры охотника/разведчика (ветка от лучника)
    /// </summary>
    static public ParametersStruct ArcherScoutParam { get { return archerScoutParam; } }
    /// <summary>
    /// Начальные параметры топового разведчика - Глаз Велеса.
    /// 100% обнаружение засады, умеет в ближний бой, хорошая защита от магии, дополнительный урон магам,
    /// высокая вероятность критического удара. Улучшает проходимость отряда по любой местность
    /// </summary>
    static public ParametersStruct ArcherEyeOfVelesParam { get { return archerEyeOfVelesParam; } }
    /// <summary>
    /// Начальные пармаетры тяжёлого лучника (или с длинным луком)
    /// </summary>
    static public ParametersStruct ArcherHeavyParam { get { return archerHeavyParam; } }
    /// <summary>
    /// Начальные параметры топового длинного лука - Жнец Рока.
    /// Уязвим для магии, высокая физическая защита, Нет штрафа дальности, двойной выстрел, высокое пробивание брони
    /// </summary>
    static public ParametersStruct ArcherReaperOfFateParam { get { return archerReaperOfFateParam; } }

    protected ParametersStruct parameters;
    /// <summary>
    /// Результирующие параметры с учётом бонусов
    /// </summary>
    public ParametersStruct Parameters { get { return SolveBonus(bonus); } set { parameters = value; } }

    protected int scale;
    /// <summary>
    /// Масштаб параметров. Используется при учёте поломки снаряжения
    /// </summary>
    public int Scale { get { return scale; } }
    /// <summary>
    /// Метод задания масштаба параметров
    /// </summary>
    /// <param name="val"></param>
    public void SetScale(int val)
    {
        scale = val;
    }
    /// <summary>
    /// Метод получения результирующих параметров по контейнеру пар: параметры, маска
    /// </summary>
    /// <param name="addParams"></param>
    /// <returns></returns>
    public ParametersStruct SolveParameters (KeyValuePair<ParametersStruct, int>[] addParams)
    {
        return Parameters.Add(addParams);
    }

    private UnitBattleParameters bonus;
    /// <summary>
    /// задание бонуса параметрам
    /// </summary>
    /// <param name="bonus"></param>
    public void AssignParametersBonus(UnitBattleParameters bonus)
    {
        this.bonus = bonus;
    }

    public UnitBattleParameters(
          MagicAttackDefenseStruct magicAttack
        , MagicAttackDefenseStruct magicDeffence
        , PhysicalAttackStruct physicalAttack
        , int physycalDefense
        , int immunityToPoison
        , int crushDefense
        , int archeryDefense
        , int armor
        , AttackTypeEnum physicalAttackType
        , int initiative
        , int fightingSpirit
        , int helth
        , int speed
        , int demage
        , int manna
        )
    {
        bonus = null;
        parameters = new ParametersStruct(
              magicAttack
            , magicDeffence
            , physicalAttack
            , physycalDefense
            , immunityToPoison
            , crushDefense
            , archeryDefense
            , armor
            , physicalAttackType
            , initiative
            , fightingSpirit
            , helth
            , speed
            , demage
            , manna
        );
    }

    /// <summary>
    /// Статический метод для выбора структуры параметров по классу юнита
    /// </summary>
    /// <param name="levelScheme"></param>
    /// <returns></returns>
    static private ParametersStruct InitBattleParameters(LevelSchemeEnum levelScheme)
    {
        switch (levelScheme)
        {
            case LevelSchemeEnum.magician_base:
                return magicianBaseParam;
            case LevelSchemeEnum.magician_battle:
                return magicianBattleParam;
            case LevelSchemeEnum.magician_fire_of_Veles:
                return magicianFireOfVelesParam;
            case LevelSchemeEnum.magician_support:
                return magicianSupportParam;
            case LevelSchemeEnum.magician_palm_of_Makosh:
                return magicianPalmOfMakoshParam;
            case LevelSchemeEnum.magician_palm_of_Morena:
                return magicianPalmOfMorenaParam;
            case LevelSchemeEnum.melee_base:
                return meleeBaseParam;
            case LevelSchemeEnum.melee_spearman:
                return meleeSpearmanParam;
            case LevelSchemeEnum.melee_spear_of_Gods:
                return meleeSpearOfGodsParam;
            case LevelSchemeEnum.melee_2hand:
                return melee2HandParam;
            case LevelSchemeEnum.melee_poleaxe_of_Veles:
                return meleePoleaxeOfVelesParam;
            case LevelSchemeEnum.melee_1hand:
                return melee1HandParam;
            case LevelSchemeEnum.melee_soldier_of_Perun:
                return meleeSoldierOfPerundParam;
            case LevelSchemeEnum.melee_double:
                return meleeDoubleParam;
            case LevelSchemeEnum.melee_rage_of_Perun:
                return meleeRageOfPerunParam;
            case LevelSchemeEnum.archer_base:
                return archerBaseParam;
            case LevelSchemeEnum.archer_scout:
                return archerScoutParam;
            case LevelSchemeEnum.archer_eye_of_Veles:
                return archerEyeOfVelesParam;
            case LevelSchemeEnum.archer_heavy:
                return archerHeavyParam;
            case LevelSchemeEnum.archer_reaper_of_Fate:
                return archerReaperOfFateParam;
        }
        return new ParametersStruct();
    }

    /// <summary>
    /// Конструктор по классу юнита
    /// </summary>
    /// <param name="levelScheme"></param>
    public UnitBattleParameters(LevelSchemeEnum levelScheme)
    {
        bonus = null;
        parameters = InitBattleParameters(levelScheme);
    }
    /// <summary>
    /// Метод применения бонуса к параметрам. Учитывает заданный масштаб параметров
    /// </summary>
    /// <param name="bonus"></param>
    /// <returns></returns>
    private ParametersStruct SolveBonus(UnitBattleParameters bonus)
    {
        ParametersStruct tmp;
        if (scale > 0 && scale < 100)
            tmp = this.parameters.Scale(scale);
        else
            tmp = this.parameters;
        if (bonus != null)
            tmp.Apply(bonus.Parameters);
        return tmp;
    }

    /// <summary>
    /// Изменение параметров по росту уровня с заданием класса юнита
    /// </summary>
    /// <param name="scheme">класс юнита</param>
    /// <param name="isNextLevelSchemeAvailabe">переключение на новый класс</param>
    public void ModifyLevelUp(LevelSchemeEnum scheme, bool isNextLevelSchemeAvailabe)
    {
        if (isNextLevelSchemeAvailabe)
        {
            parameters = InitBattleParameters(scheme);
        }
        else
        {
            LevelSchemeEnum flagCheck =
                  LevelSchemeEnum.magician_base
                | LevelSchemeEnum.magician_battle
                | LevelSchemeEnum.magician_fire_of_Veles
                | LevelSchemeEnum.magician_support
                | LevelSchemeEnum.magician_palm_of_Makosh
                | LevelSchemeEnum.magician_palm_of_Morena;
            if ((flagCheck & scheme) > 0)
            {
                parameters.ApplyMul(UnitBattleParameters.magicianParamIncr);
            }

            flagCheck =
                  LevelSchemeEnum.melee_base
                | LevelSchemeEnum.melee_spearman
                | LevelSchemeEnum.melee_spear_of_Gods
                | LevelSchemeEnum.melee_2hand
                | LevelSchemeEnum.melee_poleaxe_of_Veles
                | LevelSchemeEnum.melee_1hand
                | LevelSchemeEnum.melee_soldier_of_Perun
                | LevelSchemeEnum.melee_double
                | LevelSchemeEnum.melee_rage_of_Perun;
            if ((flagCheck & scheme) > 0)
            {
                parameters.ApplyMul(UnitBattleParameters.meleeParamIncr);
            }

            flagCheck =
                  LevelSchemeEnum.archer_base
                | LevelSchemeEnum.archer_scout
                | LevelSchemeEnum.archer_eye_of_Veles
                | LevelSchemeEnum.archer_heavy
                | LevelSchemeEnum.archer_reaper_of_Fate;
            if ((flagCheck & scheme) > 0)
            {
                parameters.ApplyMul(UnitBattleParameters.archerParamIncr);
            }
        }
    }
}