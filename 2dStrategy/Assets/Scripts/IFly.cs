﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    /// <summary>
    /// Интерфейс полёта
    /// </summary>
    public interface IFly
    {
        /// <summary>
        /// Включить возможность полёта
        /// </summary>
        void FlyPossibilitySwichOn();
        /// <summary>
        /// Отключить возможность полёта
        /// </summary>
        void FlyPossibilitySwichOff();
        /// <summary>
        /// Проверить способность летать
        /// </summary>
        /// <returns></returns>
        bool CanFlay();
    }
}
